# Translation of desktop_kdemultimedia into Japanese.
# Taiki Komoda <kom@kde.gr.jp>, 2002.
# asupara chan <asupara@star>, 2004.
# Kaori Andou <parsley@happy.email.ne.jp>, 2004.
# AWASHIRO Ikuya <ikuya@oooug.jp>, 2004.
# Toyohiro Asukai <toyohiro@ksmplus.com>, 2004.
# Shinichi Tsunoda <tsuno@ngy.1st.ne.jp>, 2005.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007, 2008, 2009, 2010.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2005, 2011.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdemultimedia\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-07 11:03+0100\n"
"PO-Revision-Date: 2011-06-11 23:51-0700\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5 stable5
#: kcmcddb/libkcddb.desktop:12
msgctxt "Name"
msgid "CDDB Retrieval"
msgstr "CDDB 検索"

#. +> trunk5 stable5
#: kcmcddb/libkcddb.desktop:63
msgctxt "GenericName"
msgid "CDDB Configuration"
msgstr "CDDB の設定"

#. +> trunk5 stable5
#: kcmcddb/libkcddb.desktop:115
msgctxt "Comment"
msgid "Configure the CDDB Retrieval"
msgstr "CDDB 検索の設定"

#. +> trunk5 stable5
#: kcmcddb/libkcddb.desktop:165
#, fuzzy
#| msgctxt "Keywords"
#| msgid "cddb"
msgctxt "Keywords"
msgid "cddb;"
msgstr "cddb"

#~ msgctxt "Keywords"
#~ msgid "cddb"
#~ msgstr "cddb"
