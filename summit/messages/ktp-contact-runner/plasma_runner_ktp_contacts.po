msgid ""
msgstr ""
"Project-Id-Version: ktp-contactrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-10-12 09:35+0200\n"
"PO-Revision-Date: 2012-03-17 00:49-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#: src/contactrunner.cpp:68
#, kde-format
msgid "Finds all IM contacts matching :q:."
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:69
#, kde-format
msgid ""
"Finds all contacts matching :q: that are capable of text chats (default "
"behavior)"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:70
#, kde-format
msgid ""
"Finds all contacts matching :q: that are capable of audio call and uses "
"audio calls as default action."
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:71
#, kde-format
msgid ""
"Finds all contacts matching :q: that are capable of video call and uses "
"video calls as default action."
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:72
#, kde-format
msgid ""
"Finds all contacts matching :q: that are capable of receiving files and "
"sends file as default action."
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:73
#, kde-format
msgid ""
"Finds all contacts matching :q: that are capable of sharing desktop and sets "
"desktop sharing as default action."
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:76
#, kde-format
msgid "Open the log viewer for :q:"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:79 src/contactrunner.cpp:411
#, kde-format
msgctxt "A keyword to change IM status"
msgid "im"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:80 src/contactrunner.cpp:412
#, kde-format
msgctxt "A keyword to change IM status"
msgid "status"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:81
#, kde-format
msgid "Change IM status"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:83 src/contactrunner.cpp:89
#, kde-format
msgctxt "Search term description"
msgid "status"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:86 src/contactrunner.cpp:88
#, kde-format
msgctxt "Description of a search term, please keep the brackets"
msgid "<status message>"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:87
#, kde-format
msgid "Change IM status and set status message."
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:92
#, kde-format
msgctxt "A command to connect IM accounts"
msgid "connect"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:93
#, kde-format
msgid "Connect IM accounts"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:94
#, kde-format
msgctxt "A command to disconnect IM accounts"
msgid "disconnect"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:95
#, kde-format
msgid "Disconnect IM accounts"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:97
#, kde-format
msgid "Start Chat"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:98
#, kde-format
msgid "Start Audio Call"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:99
#, kde-format
msgid "Start Video Call"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:100
#, kde-format
msgid "Send file(s)"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:101
#, kde-format
msgid "Share My Desktop"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:104
#, kde-format
msgid "Open the log viewer"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:232
#, kde-format
msgid "Choose files to send to %1"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:413
#, kde-format
msgctxt "A command to connect all IM accounts"
msgid "connect"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:414
#, kde-format
msgctxt "A command to disconnect all IM accounts"
msgid "disconnect"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:451
#, kde-format
msgctxt "Description of runner action"
msgid "Set IM status to online"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:452
#, kde-format
msgctxt "Description of runner subaction"
msgid "Set global IM status to online"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:457
#, kde-format
msgctxt "Description of runner action"
msgid "Set IM status to busy"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:458
#, kde-format
msgctxt "Description of runner subaction"
msgid "Set global IM status to busy"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:463
#, kde-format
msgctxt "Description of runner action"
msgid "Set IM status to away"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:464
#, kde-format
msgctxt "Description of runner subaction"
msgid "Set global IM status to away"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:469
#, kde-format
msgctxt "Description of runner action"
msgid "Set IM status to hidden"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:470
#, kde-format
msgctxt "Description of runner subaction"
msgid "Set global IM status to hidden"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:475
#, kde-format
msgctxt "Description of runner action"
msgid "Set IM status to offline"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:476
#, kde-format
msgctxt "Description of runner subaction"
msgid "Set global IM status to offline"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:492
#, kde-format
msgid "Status message: %1"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:526
#, kde-format
msgctxt "IM presence"
msgid "online"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:526
#, kde-format
msgctxt "IM presence"
msgid "available"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:530
#, kde-format
msgctxt "IM presence"
msgid "busy"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:534
#, kde-format
msgctxt "IM presence"
msgid "away"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:538
#, kde-format
msgctxt "IM presence"
msgid "hidden"
msgstr ""

#. +> trunk5 stable5
#: src/contactrunner.cpp:542
#, kde-format
msgctxt "IM presence"
msgid "offline"
msgstr ""
