# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kcm_activities\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-24 16:40+0200\n"
"PO-Revision-Date: 2019-05-11 16:16-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 19.04.0\n"

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPlugin.cpp:96 KioActivities.cpp:301
#, kde-format
msgid "Activities"
msgstr "アクティビティ"

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPlugin.cpp:99
#, kde-format
msgid "Loading..."
msgstr "読み込み中..."

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPlugin.cpp:118
#, kde-format
msgid "The Activity Manager is not running"
msgstr "Activity Manager が実行されていません"

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:53
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:125
#, kde-format
msgid "Link to the current activity"
msgstr "現在のアクティビティにリンク"

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:56
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:130
#, kde-format
msgid "Unlink from the current activity"
msgstr "現在のアクティビティからリンク解除"

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:59
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:134
#, kde-format
msgid "Link to:"
msgstr "リンク先:"

#. +> trunk5 stable5
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:64
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:142
#, kde-format
msgid "Unlink from:"
msgstr "リンク解除:"

#. +> trunk5 stable5
#: KioActivities.cpp:101 KioActivities.cpp:224
#, kde-format
msgid "Activity"
msgstr "アクティビティ"

#. +> trunk5 stable5
#: KioActivities.cpp:223
#, kde-format
msgid "Current activity"
msgstr "現在のアクティビティ"
