# Translation of kdelibs4 into Japanese.
# This file is distributed under the same license as the kdelibs package.
#
# Taiki Komoda <kom@kde.gr.jp>, 2002, 2004, 2006, 2010.
# Noboru Sinohara <shinobo@leo.bekkoame.ne.jp>, 2004.
# Toyohiro Asukai <toyohiro@ksmplus.com>, 2004.
# Kurose Shushi <md81@bird.email.ne.jp>, 2004.
# AWASHIRO Ikuya <ikuya@oooug.jp>, 2004.
# Shinichi Tsunoda <tsuno@ngy.1st.ne.jp>, 2005.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007, 2008, 2009, 2010.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2006, 2007, 2008, 2010, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kdelibs4\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-04-12 11:12+0200\n"
"PO-Revision-Date: 2011-08-27 14:05-0700\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 1.1\n"

#. +> trunk5
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#. +> trunk5
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#. +> trunk5
#: kpackagelauncherqml/main.cpp:30
#, kde-format
msgid "KPackage QML application shell"
msgstr ""

#. +> trunk5
#: kpackagelauncherqml/main.cpp:38
#, kde-format
msgid "The unique name of the application (mandatory)"
msgstr ""

#. +> trunk5
#: qmlcontrols/kcmcontrols/qml/ContextualHelpButton.qml:62
#, kde-format
msgctxt "@action:button"
msgid "Show Contextual Help"
msgstr ""

#. +> trunk5
#: qmlcontrols/kcmcontrols/qml/private/GridViewInternal.qml:79
#, kde-format
msgid "No items found"
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/ColorButton.qml:63
#, kde-format
msgctxt "@info:whatsthis for a button"
msgid "Color button"
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/ColorButton.qml:65
#, kde-format
msgctxt "@info:whatsthis for a button of current color code %1"
msgid "Current color is %1. This button will open a color chooser dialog."
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/ColorButton.qml:66
#, kde-format
msgctxt "@info:whatsthis for a button of current color code %1"
msgid "Current color is %1."
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/KeySequenceItem.qml:75
#, kde-format
msgctxt "What the user inputs now will be taken as the new shortcut"
msgid "Input"
msgstr "入力"

#. +> trunk5
#: qmlcontrols/kquickcontrols/KeySequenceItem.qml:76
#, kde-format
msgctxt "No shortcut defined"
msgid "None"
msgstr "なし"

#. +> trunk5
#: qmlcontrols/kquickcontrols/KeySequenceItem.qml:89
#, kde-format
msgid ""
"Click on the button, then enter the shortcut like you would in the program.\n"
"Example for Ctrl+A: hold the Ctrl key and press A."
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:118
#, kde-format
msgid "Reserved Shortcut"
msgstr "予約済みのショートカット"

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:120
#, kde-format
msgid ""
"The F12 key is reserved on Windows, so cannot be used for a global "
"shortcut.\n"
"Please choose another one."
msgstr ""
"F12 は Windows で予約済みになっているため、グローバルショートカットには使えま"
"せん。\n"
"他のキーを選択してください。"

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:144
#, kde-format
msgid "Global Shortcut Shadowing"
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:147
#, kde-format
msgid "The '%1' key combination is shadowed by following global actions:\n"
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:149
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:155
#, kde-format
msgid "Action '%1' in context '%2'\n"
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:153
#, kde-format
msgid "The '%1' key combination shadows following global actions:\n"
msgstr ""

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:193
#, kde-format
msgid "Conflict with Standard Application Shortcut"
msgstr "標準アプリケーションショートカットと競合します"

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:195
#, kde-format
msgid ""
"The '%1' key combination is also used for the standard action \"%2\" that "
"some applications use.\n"
"Do you really want to use it as a global shortcut as well?"
msgstr ""
"キーの組み合わせ %1 は、複数のアプリケーションが使用する標準アクション「%2」"
"に割り当てられています。\n"
"本当にこれをグローバルショートカットとしても使いますか？|/|キーの組み合わせ "
"%1 は、複数のアプリケーションが使用する標準アクション「$[~stripAccel "
"$[~getForm %2 ~full]]」に割り当てられています。\n"
"本当にこれをグローバルショートカットとしても使いますか？"

#. +> trunk5
#: qmlcontrols/kquickcontrols/private/keysequencehelper.cpp:201
#, kde-format
msgid "Reassign"
msgstr "割り当て直す"

#. +> trunk5
#: quickaddons/configmodule.cpp:187
#, kde-format
msgid "Invalid KPackage '%1'"
msgstr ""

#. +> trunk5
#: quickaddons/configmodule.cpp:193
#, kde-format
msgid "No QML file provided"
msgstr ""

#~ msgid "The key you just pressed is not supported by Qt."
#~ msgstr "あなたが押したキーは Qt でサポートされていません。"

#~ msgid "Unsupported Key"
#~ msgstr "サポートされていないキー"

#, fuzzy
#~| msgctxt "No shortcut defined"
#~| msgid "None"
#~ msgid "None"
#~ msgstr "なし"

#~ msgid "Undo"
#~ msgstr "元に戻す"

#~ msgid "Redo"
#~ msgstr "やり直す"

#~ msgid "Cut"
#~ msgstr "切り取り"

#~ msgid "Copy"
#~ msgstr "コピー"

#~ msgid "Paste"
#~ msgstr "貼り付け"

#~ msgid "Delete"
#~ msgstr "削除"

#, fuzzy
#~| msgctxt "@action"
#~| msgid "Clear"
#~ msgid "Clear"
#~ msgstr "クリア"

#, fuzzy
#~| msgctxt "@action"
#~| msgid "Select All"
#~ msgid "Select All"
#~ msgstr "すべて選択"
