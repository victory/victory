msgid ""
msgstr ""
"Project-Id-Version: k3b.appdata\n"
"POT-Creation-Date: 2017-03-23 09:20+0100\n"
"PO-Revision-Date: 2014-05-01 01:02-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. (itstool) path: component/name
#. +> trunk5 stable5
#: org.kde.k3b.appdata.xml:5
msgid "K3b"
msgstr ""

#. (itstool) path: component/summary
#. +> trunk5 stable5
#: org.kde.k3b.appdata.xml:6
msgid "Disk Burning"
msgstr ""

#. (itstool) path: description/p
#. +> trunk5 stable5
#: org.kde.k3b.appdata.xml:8
msgid ""
"K3b was created to be a feature-rich and easy to handle CD burning "
"application. It consists of basically three parts:"
msgstr ""

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.k3b.appdata.xml:13
msgid ""
"The projects: Projects are created from the file menu and then filled with "
"data to burn"
msgstr ""

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.k3b.appdata.xml:17
msgid ""
"The Tools: The tools menu offers different tools like CD copy or DVD "
"formatting"
msgstr ""

#. (itstool) path: ul/li
#. +> trunk5 stable5
#: org.kde.k3b.appdata.xml:21
msgid ""
"Context sensitive media actions: When clicking on the Icon representing a CD/"
"DVD drive K3b will present its contents and allow some further action. This "
"is for example the way to rip audio CDs"
msgstr ""
