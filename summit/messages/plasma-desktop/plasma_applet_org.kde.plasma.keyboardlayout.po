msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-20 15:50+0200\n"
"PO-Revision-Date: 2021-01-17 17:47-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5 plasma5lts
#: contents/config/config.qml:6
#, kde-format
msgid "General"
msgstr ""

#. +> trunk5 stable5 plasma5lts
#: contents/ui/configGeneral.qml:17
#, kde-format
msgid "Display style:"
msgstr ""

#. +> trunk5 stable5 plasma5lts
#: contents/ui/configGeneral.qml:43
#, kde-format
msgid "Layouts:"
msgstr ""

#. +> trunk5 stable5 plasma5lts
#: contents/ui/configGeneral.qml:44
#, kde-format
msgid "Configure…"
msgstr ""
