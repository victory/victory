msgid ""
msgstr ""
"Project-Id-Version: akonadi_ews_resource\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-24 10:19+0100\n"
"PO-Revision-Date: 2017-10-04 13:39-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#: ewsautodiscoveryjob.cpp:44 ewsautodiscoveryjob.cpp:51
#, kde-format
msgid "Incorrect email address"
msgstr ""

#. +> trunk5 stable5
#: ewsautodiscoveryjob.cpp:114
#, kde-format
msgid "Exchange protocol information not found"
msgstr ""

#. +> trunk5 stable5
#: ewsclient/auth/ewsoauth.cpp:515
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Microsoft Exchange credentials for the account <b>%1</b> are no longer "
"valid. You need to authenticate in order to continue using it."
msgstr ""

#. +> trunk5 stable5
#: ewsclient/auth/ewsoauth.cpp:524
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Failed to obtain credentials for Microsoft Exchange account <b>%1</b>. "
"Please update it in the account settings page."
msgstr ""

#. +> trunk5 stable5
#: ewsclient/auth/ewspasswordauth.cpp:57
#, kde-format
msgctxt "@info"
msgid ""
"The username/password for the Microsoft Exchange EWS account <b>%1</b> is "
"not valid. Please update it in the account settings page."
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:76
#, kde-format
msgctxt "@title:window"
msgid "Microsoft Exchange Configuration"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:101
#, kde-format
msgctxt "Server status"
msgid "OK"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:138
#, kde-format
msgctxt "User Agent"
msgid "Custom"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:149
#, kde-format
msgctxt "@info"
msgid "Akonadi Resource for Microsoft Exchange Web Services (EWS)"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:150
#, kde-format
msgctxt "@info"
msgid "Copyright (c) Krzysztof Nowicki 2015-2020"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:151
#, kde-format
msgctxt "@info"
msgid "Version %1"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:152
#, kde-format
msgctxt "@info"
msgid ""
"Distributed under the GNU Library General Public License version 2.0 or "
"later."
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:247
#, kde-format
msgctxt "Exchange server autodiscovery"
msgid "Autodiscovery failed"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:262
#, kde-format
msgctxt "Exchange server connection"
msgid "Connection failed"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:263 ewsconfigdialog.cpp:416
#, kde-format
msgctxt "Exchange server status"
msgid "Failed"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:264 ewsconfigdialog.cpp:417
#, kde-format
msgctxt "Exchange server version"
msgid "Unknown"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:267 ewsconfigdialog.cpp:421
#, kde-format
msgctxt "Exchange server status"
msgid "OK"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:336
#, kde-format
msgid ""
"Autodiscovery failed. This can be caused by incorrect parameters. Do you "
"still want to save your settings?"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:337
#, kde-format
msgid "Exchange server autodiscovery"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:365
#, kde-format
msgid ""
"Connecting to Exchange failed. This can be caused by incorrect parameters. "
"Do you still want to save your settings?"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:366
#, kde-format
msgid "Exchange server connection"
msgstr ""

#. +> trunk5 stable5
#: ewsconfigdialog.cpp:418
#, kde-format
msgid "Connection failed"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, SetupServerView)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:14
#, kde-format
msgid "EWS Configuration"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:24
#, kde-format
msgctxt "General settings tab"
msgid "General"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, accountInfoGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:45
#, kde-format
msgid "Account Information"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, accountNameLabel)
#. i18n: ectx: property (text), widget (QLabel, accountLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:57 ewsmtaconfigdialog.ui:32
#, kde-format
msgid "Account Name:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QLineEdit, accountName)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:64
#, kde-format
msgid "This is the account name that will appear in all Akonadi applications"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, emailLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:71
#, kde-format
msgctxt "Exchange account e-mail address"
msgid "Email:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QLineEdit, kcfg_Email)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:78
#, kde-format
msgid "The e-mail address of this account"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, authenticationGroupBox)
#. i18n: ectx: property (title), widget (QGroupBox, advAuthenticationGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:88 ewsconfigdialog.ui:492
#, kde-format
msgid "Authentication"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QRadioButton, authUsernameRadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:94
#, kde-format
msgid ""
"<html><head/><body><p>Enables traditional username/password authentication "
"against the Exchange server. This includes methods like Basic, NTLM or "
"Kerberos.</p><p>This authentication method is considered to be somewhat "
"legacy and is mostly used for smaller on-premise Exchange installations and "
"hosted Office 365 instances, where the administrator has chosen to enable it."
"</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, authUsernameRadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:97
#, kde-format
msgctxt "authentication type"
msgid "&Username/Password"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_HasDomain)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:143
#, kde-format
msgid ""
"<html><head/><body><p>Allows to specify an Active Directory domain for the "
"user.</p><p><span style=\" font-weight:600;\">Note:</span> There is a subtle "
"difference between &quot;empty domain&quot; and &quot;no-domain&quot;.</"
"p><p>With an empty domain (Domain checkbox checked, empty domain in edit "
"box) the EWS resource will pass the user with an explicitly empty domain "
"(&quot;\\username&quot;).</p><p>With no domain set (Domain checkbox cleared) "
"the username will be passed without the domain (&quot;username&quot;), which "
"will cause the server to set the domain to the default one its configured "
"with.</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_HasDomain)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:146
#, kde-format
msgid "Domain:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QLineEdit, kcfg_Domain)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:156
#, kde-format
msgid "The Active Directory domain the user belongs to."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, usernameLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:169
#, kde-format
msgctxt "Exchange account username/login"
msgid "Username:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, passwordLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:179
#, kde-format
msgid "Password:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (KPasswordLineEdit, passwordEdit)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:186
#, kde-format
msgid ""
"<html><head/><body><p>The password used to login to the Exchange server.</"
"p><p><span style=\" font-weight:600;\">Note:</span> This can be left empty "
"when using Kerberos authentication.</p></body></html>"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QLineEdit, kcfg_Username)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:193
#, kde-format
msgid ""
"<html><head/><body><p>The username used to login to the Exchange server.</"
"p><p><span style=\" font-weight:600;\">Note:</span> This can be left empty "
"when using Kerberos authentication.</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, kerberosNoteLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:203
#, kde-format
msgid ""
"<html><head/><body><p><span style=\" font-weight:600;\">Note:</span> "
"Username and password are not needed (can be empty) when Kerberos is "
"properly configured on your system.</p></body></html>"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QRadioButton, authOAuth2RadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:222
#, kde-format
msgid ""
"<html><head/><body><p>Enables more modern and stronger authentication based "
"on OAuth2.</p><p>This type of authentication will open an organization-"
"supplied webpage, where the user will be able to enter authentication "
"information, which may also include multifactor authentication in form of "
"one-time-passwords generated by tokens.</p><p>This method is mostly used for "
"hosted Office 365 instances.</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, authOAuth2RadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:225
#, kde-format
msgid "OAuth&2 (Office 365)"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, ewsSettingsGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:235
#, kde-format
msgid "EWS Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, serverUrlLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:250
#, kde-format
msgid "EWS URL:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QLineEdit, kcfg_BaseUrl)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:263
#, kde-format
msgid "The Microsoft Exchange server URL used to send EWS requests."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_AutoDiscovery)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:285
#, kde-format
msgid ""
"<html><head/><body><p>Attempts to find out the Microsoft Exchange server "
"address relevant to the supplied e-mail address.</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_AutoDiscovery)
#. i18n: ectx: label, entry (AutoDiscovery), group (General)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:288 ewsresource.kcfg:34
#, kde-format
msgid "Server Autodiscovery"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, autodiscoverButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:298
#, kde-format
msgid "Perform"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, tryConnectButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:308
#, kde-format
msgid "Try connect"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QGroupBox, pkeyAuthGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:324
#, kde-format
msgid ""
"<html><head/><body><p>Private Key (PKey) Authentication is used as an "
"additional authentication step to OAuth2-based authentication. It uses an "
"X.509 certificate and private key that identifies a trusted device, which "
"was joined to the Azure Directory using a Workplace Join.</p><p>Some "
"administrators may mandate use of PKey authentication when connecting to "
"Office 365 from outside of corporate network. In other cases, when multi-"
"factor authentication is set-up, using the PKey authentication can satisfy "
"requirements for strong authentication, in which case entering the MFA code "
"is not required.</p></body></html>"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, pkeyAuthGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:327
#, kde-format
msgid "Private Key (PKey) Authentication (Office 365)"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pkeyAuthCertLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:339
#, kde-format
msgid "Certificate:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pkeyAuthKeyLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:356
#, kde-format
msgid "Private key:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pkeyAuthPasswordLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:370
#, kde-format
msgid "Key password:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, subscriptionTab)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:401
#, kde-format
msgid "Subscriptions"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:407
#, kde-format
msgctxt "Advanced settings tab"
msgid "Advanced"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, retrievalMethodGroupBox)
#. i18n: ectx: label, entry (RetrievalMethod), group (General)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:413 ewsresource.kcfg:44
#, kde-format
msgid "Retrieval"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QRadioButton, pollRadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:419
#, kde-format
msgid ""
"<html><head/><body><p>Periodically asks for new item change events.</p></"
"body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, pollRadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:422
#, kde-format
msgid "Po&ll for new messages"
msgstr ""

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_PollInterval)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:447
#, kde-format
msgid " minutes"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, pollIntervalLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:466
#, kde-format
msgid "Interval"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QRadioButton, streamingRadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:476
#, kde-format
msgid ""
"<html><head/><body><p>Streaming notifications behave similarly to push mode "
"- a connection is kept alive and notifications about item changes are "
"delivered instantly.</p><p><br/></p><p>This option requires Exchange server "
"version 2010 SP1 or later.</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, streamingRadioButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:479
#, kde-format
msgid "&Use streaming notifications"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QCheckBox, kcfg_EnableNTLMv2)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:498
#, kde-format
msgid ""
"<html><head/><body><p>By default KDE uses the less secure NTLMv1 "
"authentication. Enabling this will cause the more secure NTLMv2 protocol to "
"be used.</p><p>Some newer Microsoft Exchange servers are configured to only "
"allow NTLMv2 authentication. In such case this option must be enabled in "
"order to connect successfully.</p></body></html>"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_EnableNTLMv2)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:501
#, kde-format
msgid "Enable NTLMv2"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QGroupBox, userAgentGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:511
#, kde-format
msgid ""
"<html><head/><body><p>Some organizations restrict access to mail only to "
"approved clients, such as Microsoft Outlook. On the server-side this is "
"implemented by analyzing the User-Agent string and only allowing a list of "
"approved clients.</p><p>This setting allows to emulate some well known "
"clients as well as providing a custom User-Agent string.</p><p>Please be "
"advised that bypassing company policies may result in personal consequences."
"</p></body></html>"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, userAgentGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:514
#, kde-format
msgid "Use custo&m User Agent"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, clearSyncStateGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:539
#, kde-format
msgid "Clear Synchronization State"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, clearSyncStateLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:545
#, kde-format
msgid ""
"Resets synchronization state between Exchange and Akonadi. Use in case some "
"items or folders are not showing up despite existing in Exchange."
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, clearFolderTreeSyncStateButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:570
#, kde-format
msgid "Clear Folder Tree State"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, clearFolderItemSyncStateButton)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:577
#, kde-format
msgid "Clear Folder Item State"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, statusTab)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:604
#, kde-format
msgid "Status"
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, serverStatusGroupBox)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:610
#, kde-format
msgid "Server Status"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, serverStatusLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:616
#, kde-format
msgid "Status:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, serverStatusText)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:623
#, kde-format
msgctxt "Unknown server status"
msgid "Unknown"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, serverVersionLabel)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:633
#, kde-format
msgid "Version:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, serverVersionText)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:640
#, kde-format
msgctxt "Unknown server version"
msgid "Unknown"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, aboutTab)
#. +> trunk5 stable5
#: ewsconfigdialog.ui:664
#, kde-format
msgid "About"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitempayloadjob.cpp:63
#, kde-format
msgctxt "@info:status"
msgid "Failed to process items retrieval request"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitempayloadjob.cpp:71 ewsresource.cpp:335 ewsresource.cpp:340
#: ewsresource.cpp:531
#, kde-format
msgctxt "@info:status"
msgid "Failed to retrieve items"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitempayloadjob.cpp:78
#, kde-format
msgctxt "@info:status"
msgid "Failed to retrieve items - incorrect number of responses"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitempayloadjob.cpp:91
#, kde-format
msgctxt "@info:status"
msgid "Failed to retrieve items - Akonadi item not found for item %1"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitempayloadjob.cpp:98
#, kde-format
msgctxt "@info:status"
msgid "Failed to retrieve items - Unknown item type for item %1"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitempayloadjob.cpp:104
#, kde-format
msgctxt "@info:status"
msgid "Failed to fetch item payload"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitemsjob.cpp:99
#, kde-format
msgctxt "@info:status"
msgid "Retrieving %1 item list"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitemsjob.cpp:215
#, kde-format
msgctxt "@info:status"
msgid "Retrieving %1 item list (%2 items)"
msgstr ""

#. +> trunk5 stable5
#: ewsfetchitemsjob.cpp:395
#, kde-format
msgctxt "@info:status"
msgid "Retrieving %1 items"
msgstr ""

#. +> trunk5 stable5
#: ewsmtaconfigdialog.cpp:45
#, kde-format
msgctxt "@title:window"
msgid "Microsoft Exchange Mail Transport Configuration"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, SetupServerView)
#. +> trunk5 stable5
#: ewsmtaconfigdialog.ui:14
#, kde-format
msgid "EWS Mail Transport Configuration"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, recourceLabel)
#. +> trunk5 stable5
#: ewsmtaconfigdialog.ui:42
#, kde-format
msgid "Parent Exchange account:"
msgstr ""

#. +> trunk5 stable5
#: ewsmtaresource.cpp:58
#, kde-format
msgid "Unable to connect to master EWS resource"
msgstr ""

#. i18n: ectx: label, entry (EwsResource), group (General)
#. +> trunk5 stable5
#: ewsmtaresource.kcfg:10
#, kde-format
msgid "Ews Resource"
msgstr ""

#. i18n: ectx: tooltip, entry (EwsResource), group (General)
#. i18n: ectx: whatsthis, entry (EwsResource), group (General)
#. +> trunk5 stable5
#: ewsmtaresource.kcfg:11 ewsmtaresource.kcfg:12
#, kde-format
msgid "The name of the parent EWS Resource to use when sending mail"
msgstr ""

#. +> trunk5 stable5
#: ewsprogressdialog.cpp:34
#, kde-format
msgid "Cancel"
msgstr ""

#. +> trunk5 stable5
#: ewsprogressdialog.cpp:48
#, kde-format
msgctxt "@title:window"
msgid "Exchange server autodiscovery"
msgstr ""

#. +> trunk5 stable5
#: ewsprogressdialog.cpp:49
#, kde-format
msgid "Performing Microsoft Exchange server autodiscovery..."
msgstr ""

#. +> trunk5 stable5
#: ewsprogressdialog.cpp:52
#, kde-format
msgctxt "@title:window"
msgid "Connecting to Exchange"
msgstr ""

#. +> trunk5 stable5
#: ewsprogressdialog.cpp:53
#, kde-format
msgid "Connecting to Microsoft Exchange server..."
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:145
#, kde-format
msgctxt "@info:status"
msgid "Connecting to Exchange server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:163 ewsresource.cpp:170 ewsresource.cpp:177
#: ewsresource.cpp:315
#, kde-format
msgctxt "@info:status"
msgid "Unable to connect to Exchange server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:255
#, kde-format
msgctxt "@info:status"
msgid "Root folder id not known."
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:259
#, kde-format
msgctxt "@info:status"
msgid "Retrieving collection tree"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:408
#, kde-format
msgctxt "@info:status"
msgid "Retrieving items"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:430 ewsresource.cpp:516
#, kde-format
msgctxt "@info:status"
msgid "Failed to retrieve items - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:464
#, kde-format
msgctxt "@info:status"
msgid "Failed to retrieve folders - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:470
#, kde-format
msgctxt "@info:status"
msgid "Failed to process folders retrieval request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:487
#, kde-format
msgctxt "@info:status"
msgid "Invalid incremental folders retrieval request job object"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:550
#, kde-format
msgctxt "@info:status"
msgid "Item type not supported for changing"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:563
#, kde-format
msgctxt "@info:status"
msgid "Updating item flags"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:575
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item flags update request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:582
#, kde-format
msgctxt "@info:status"
msgid "Failed to update item flags - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:596
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item update request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:603
#, kde-format
msgctxt "@info:status"
msgid "Failed to update item - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:638
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item move request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:645
#, kde-format
msgctxt "@info:status"
msgid "Failed to move item - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:652
#, kde-format
msgctxt "@info:status"
msgid "Failed to move item - invalid number of responses received from server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:722
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item delete request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:729
#, kde-format
msgctxt "@info:status"
msgid "Failed to delete item - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:736
#, kde-format
msgctxt "@info:status"
msgid ""
"Failed to delete item - invalid number of responses received from server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:781 ewsresource.cpp:994
#, kde-format
msgctxt "@info:status"
msgid "Item type not supported for creation"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:792
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item create request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:798
#, kde-format
msgctxt "@info:status"
msgid "Failed to create item - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:819
#, kde-format
msgctxt "@info:status"
msgid "Failed to add collection - cannot determine EWS folder type"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:838
#, kde-format
msgctxt "@info:status"
msgid "Failed to process folder create request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:844
#, kde-format
msgctxt "@info:status"
msgid "Failed to create folder - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:856
#, kde-format
msgctxt "@info:status"
msgid "Failed to create folder"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:879
#, kde-format
msgctxt "@info:status"
msgid "Failed to process folder move request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:885
#, kde-format
msgctxt "@info:status"
msgid "Failed to move folder - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:891
#, kde-format
msgctxt "@info:status"
msgid ""
"Failed to move folder - invalid number of responses received from server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:902
#, kde-format
msgctxt "@info:status"
msgid "Failed to move folder"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:930
#, kde-format
msgctxt "@info:status"
msgid "Failed to process folder update request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:936
#, kde-format
msgctxt "@info:status"
msgid "Failed to update folder - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:942
#, kde-format
msgctxt "@info:status"
msgid ""
"Failed to update folder - invalid number of responses received from server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:953
#, kde-format
msgctxt "@info:status"
msgid "Failed to update folder"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:970
#, kde-format
msgctxt "@info:status"
msgid "Failed to process folder delete request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:976
#, kde-format
msgctxt "@info:status"
msgid "Failed to delete folder - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:984
#, kde-format
msgctxt "@info:status"
msgid "Failed to delete folder"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1008 ewsresource.cpp:1042
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item send request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1014 ewsresource.cpp:1048
#, kde-format
msgctxt "@info:status"
msgid "Failed to send item - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1053
#, kde-format
msgctxt "@info:status"
msgid "Invalid number of responses received from server"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1245
#, kde-format
msgctxt "@info:status"
msgid "Updating item tags"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1258
#, kde-format
msgctxt "@info:status"
msgid "Failed to process item tags update request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1264
#, kde-format
msgctxt "@info:status"
msgid "Failed to update item tags - internal error"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1301
#, kde-format
msgctxt "@info:status"
msgid "Failed to process global tag update request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1317
#, kde-format
msgctxt "@info:status"
msgid "Failed to process global tags retrieval request"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1388
#, kde-format
msgctxt "@action:button"
msgid "Authenticate"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1402 ewsresource.cpp:1417
#, kde-format
msgctxt "@info:status"
msgid "Authentication failed"
msgstr ""

#. +> trunk5 stable5
#: ewsresource.cpp:1425
#, kde-format
msgctxt "@info:status Resource is ready"
msgid "Ready"
msgstr ""

#. i18n: ectx: label, entry (BaseUrl), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:10
#, kde-format
msgid "Server URL"
msgstr ""

#. i18n: ectx: tooltip, entry (BaseUrl), group (General)
#. i18n: ectx: whatsthis, entry (BaseUrl), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:11 ewsresource.kcfg:12
#, kde-format
msgid ""
"The URL of the Microsoft Exchange server, should be something like https://"
"myserver.org/EWS/Exchange.asmx"
msgstr ""

#. i18n: ectx: label, entry (Username), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:15
#, kde-format
msgctxt "The username to login into the server"
msgid "Username"
msgstr ""

#. i18n: ectx: tooltip, entry (Username), group (General)
#. i18n: ectx: whatsthis, entry (Username), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:16 ewsresource.kcfg:17
#, kde-format
msgid "The username that is used to log into the Microsoft Exchange server"
msgstr ""

#. i18n: ectx: label, entry (Domain), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:20
#, kde-format
msgid "Domain"
msgstr ""

#. i18n: ectx: tooltip, entry (Domain), group (General)
#. i18n: ectx: whatsthis, entry (Domain), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:21 ewsresource.kcfg:22
#, kde-format
msgid "The Active Directory domain the user belongs to"
msgstr ""

#. i18n: ectx: label, entry (HasDomain), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:25
#, kde-format
msgid "Use a domain name during authentication"
msgstr ""

#. i18n: ectx: label, entry (Email), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:29
#, kde-format
msgctxt "Account e-mail address"
msgid "E-mail"
msgstr ""

#. i18n: ectx: tooltip, entry (Email), group (General)
#. i18n: ectx: whatsthis, entry (Email), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:30 ewsresource.kcfg:31
#, kde-format
msgid "The primary e-mail address of this account"
msgstr ""

#. i18n: ectx: tooltip, entry (AutoDiscovery), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:35
#, kde-format
msgid "Attempt to automatically discover Exchange server address"
msgstr ""

#. i18n: ectx: whatsthis, entry (AutoDiscovery), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:36
#, kde-format
msgid ""
"Attempts to find out the Microsoft Exchange server address relevant to the "
"supplied e-mail address."
msgstr ""

#. i18n: ectx: label, entry (PollInterval), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:39
#, kde-format
msgid "Poll Interval"
msgstr ""

#. i18n: ectx: tooltip, entry (PollInterval), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:40
#, kde-format
msgid "Sets the interval for checking for new mail"
msgstr ""

#. i18n: ectx: whatsthis, entry (PollInterval), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:41
#, kde-format
msgid "Sets the interval for checking for new mail."
msgstr ""

#. i18n: ectx: label, entry (ServerSubscription), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:47
#, kde-format
msgid "Enable Server-Side Subscriptions"
msgstr ""

#. i18n: ectx: label, entry (ServerSubscriptionList), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:50
#, kde-format
msgid "List of folders to subscribe to"
msgstr ""

#. i18n: ectx: label, entry (EnableNTLMv2), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:54
#, kde-format
msgid "Enable NTLMv2 authentication"
msgstr ""

#. i18n: ectx: label, entry (UserAgent), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:58
#, kde-format
msgid "Forces a non-default User-Agent string"
msgstr ""

#. i18n: ectx: label, entry (OAuth2AppId), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:64
#, kde-format
msgid "OAuth2 application identifier"
msgstr ""

#. i18n: ectx: label, entry (OAuth2ReturnUri), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:68
#, kde-format
msgid "OAuth2 return URI"
msgstr ""

#. i18n: ectx: label, entry (PKeyCert), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:72
#, kde-format
msgid "Path to PKey authentication PEM certificate"
msgstr ""

#. i18n: ectx: label, entry (PKeyKey), group (General)
#. +> trunk5 stable5
#: ewsresource.kcfg:75
#, kde-format
msgid "Path to PKey authentication PEM private key"
msgstr ""

#. +> trunk5 stable5
#: ewssettings.cpp:90
#, kde-format
msgid "Please enter password for user '%1' and Exchange account '%2'."
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:171
#, kde-format
msgctxt "@info"
msgid "Exchange server not configured."
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:180 ewssubscriptionwidget.cpp:201
#, kde-format
msgctxt "@info"
msgid "Failed to retrieve folder list."
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:286
#, kde-format
msgctxt "@option:check"
msgid "Enable server-side subscriptions"
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:294
#, kde-format
msgctxt "@label:textbox"
msgid "Filter folders"
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:314
#, kde-format
msgctxt "@action:button"
msgid "Reload &List"
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:317
#, kde-format
msgctxt "@action:button"
msgid "&Reset"
msgstr ""

#. +> trunk5 stable5
#: ewssubscriptionwidget.cpp:326
#, kde-format
msgctxt "@option:check"
msgid "Subscribed only"
msgstr ""

#. +> trunk5 stable5
#: mail/ewscreatemailjob.cpp:148 mail/ewscreatemailjob.cpp:185
#: mail/ewscreatemailjob.cpp:218
#, kde-format
msgid "Failed to create mail item"
msgstr ""

#. +> trunk5 stable5
#: mail/ewsmodifymailjob.cpp:92
#, kde-format
msgctxt "@info:status"
msgid "Item update failed: "
msgstr ""

#. +> trunk5 stable5
#: mail/ewsmodifymailjob.cpp:103
#, kde-format
msgctxt "@info:status"
msgid "Item out of order while processing update item response."
msgstr ""
