# Tsugumi Harabayashi <tu.hrby@gmail.com>, 2014.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: TemplateShape\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-25 11:04+0200\n"
"PO-Revision-Date: 2014-11-29 22:57-0800\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 1.5\n"

#. +> trunk5 stable5
#: ChangeSomethingCommand.cpp:34
msgctxt "(qtundo-format)"
msgid "Change something"
msgstr ""

#. +> trunk5 stable5
#: TemplateShapeFactory.cpp:32
#, kde-format
msgid "Template shape"
msgstr "テンプレートシェイプ"

#. +> trunk5 stable5
#: TemplateShapeFactory.cpp:34
#, kde-format
msgid "Simple shape that is used as a template for developing other shapes."
msgstr ""
"シェイプを作成する時のテンプレートとして使用できるシンプルなシェイプです"

#. +> trunk5 stable5
#: TemplateTool.cpp:77
#, kde-format
msgid "Open"
msgstr "開く"

#. +> trunk5 stable5
#: TemplateToolFactory.cpp:26
#, kde-format
msgid "Template shape editing"
msgstr "テンプレートシェイプの編集"

#, fuzzy
#~| msgid "Template shape"
#~ msgctxt "(qtundo-format)"
#~ msgid "Template shape"
#~ msgstr "テンプレートシェイプ"

#, fuzzy
#~| msgid ""
#~| "Simple shape that is used as a template for developing other shapes."
#~ msgctxt "(qtundo-format)"
#~ msgid "Simple shape that is used as a template for developing other shapes."
#~ msgstr ""
#~ "シェイプを作成する時のテンプレートとして使用できるシンプルなシェイプです"

#, fuzzy
#~| msgid "Open"
#~ msgctxt "(qtundo-format)"
#~ msgid "Open"
#~ msgstr "開く"

#, fuzzy
#~| msgid "Template shape editing"
#~ msgctxt "(qtundo-format)"
#~ msgid "Template shape editing"
#~ msgstr "テンプレートシェイプの編集"

#~ msgid "Template shape editing tool"
#~ msgstr "テンプレートシェイプ編集ツール"
