# bbch <suzihi248@ruru.be>, 2020.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-24 12:16+0200\n"
"PO-Revision-Date: 2020-06-13 18:02-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 20.04.0\n"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:1
msgid "An introduction for using clipping masks in Krita."
msgstr "Krita でクリッピングマスクを使用する方法を紹介します。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:10
msgid "Alpha Inheritance"
msgstr "アルファ継承"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:10
msgid "Clipping Masks"
msgstr "クリッピングマスク"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:15
msgid "Clipping Masks and Alpha Inheritance"
msgstr "クリッピングマスクとアルファ継承"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:17
msgid ""
"Krita doesn't have clipping mask functionality in the manner that Photoshop "
"and programs that mimic Photoshop's functionality have. That's because in "
"Krita, unlike such software, a group layer is not an arbitrary collection of "
"layers. Rather, in Krita, group layers are composited separately from the "
"rest of the stack, and then the result is added into the stack. In other "
"words, in Krita group layers are in effect distinct images inside your image."
msgstr ""
"Krita は、Photoshop や、それを模倣したプログラムと同じ仕組みのクリッピングマ"
"スクは存在しません。Krita では、それらのソフトウェアとは異なり、グループレイ"
"ヤーが任意レイヤーの集合ではないからです。ただしくは Krita でのグループレイ"
"ヤーは、スタックの他の部分とは別に合成され、その結果がスタックに追加されま"
"す。つまり、Krita では、グループレイヤーは実際にはイメージ内の別個のイメージ"
"です。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:24
msgid ""
"The exception is when using pass-through mode, meaning that alpha "
"inheritance won't work right when turning on pass-through on the layer."
msgstr ""
"ただし、パススルーモードを使用している場合は例外で、レイヤーでパススルーを有"
"効にしてもアルファ継承は正常に動作しません。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:30
msgid ""
"When we turn on alpha inheritance, the alpha-inherited layer keeps the same "
"transparency as the layers below."
msgstr ""
"アルファ継承を有効にすると、アルファ継承されたレイヤーはその下のレイヤーと同"
"じ透明度を維持します。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:36
msgid ""
"Combined with group layers this can be quite powerful. A situation where "
"this is particularly useful is the following:"
msgstr ""
"グループレイヤーと組み合わせると、非常に強力になります。これが特に役立つ場面"
"は、次のとおりです。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:42
msgid ""
"Here we have an image with line art and a layer for each flat of colors. We "
"want to add complicated multi-layered shading to this, while keeping the "
"neatness of the existing color flats. To get a clipping mask working, you "
"first need to put layers into a group. You can do this by making a group "
"layer and drag-and-dropping the layers into it, or by selecting the layers "
"you want grouped and pressing the :kbd:`Ctrl + G` shortcut. Here we do that "
"with the iris and the eye-white layers."
msgstr ""
"ここには、線画とフラットカラーごとのレイヤーを含む画像があります。既存のフ"
"ラットカラーの領域を保ちながら、複数層による複雑な陰影を追加したいと思いま"
"す。クリッピングマスクを機能させるには、まずレイヤーを一つのグループに入れる"
"必要があります。これを行うには、グループレイヤーを作成して、その中にレイヤー"
"をドラッグ&ドロップするか、グループ化するレイヤーを選択して、キーボードショー"
"トカット :kbd:`Ctrl + G` を押します。ここでは、虹彩と白目のレイヤーを使って行"
"います。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:50
msgid ""
"We add a layer for the highlight above the other two layers, and add some "
"white scribbles."
msgstr ""
"他の2つのレイヤー上にハイライト用のレイヤーを追加し、白い落書きを追加します。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:60
msgid ""
"In the above, we have our layer with a white scribble on the left, and on "
"the right, the same layer, but with alpha inheritance active, limiting it to "
"the combined area of the iris and eye-white layers."
msgstr ""
"上の例では、左側に白い落書きのあるレイヤーと右側に同じレイヤーがありますが、"
"アルファ継承が有効になっているため、虹彩と白目のレイヤーを合わせた領域に表示"
"が限定されます。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:66
msgid ""
"Now there’s an easier way to set up alpha inheritance. If you select a layer "
"or set of layers and press the :kbd:`Ctrl + Shift + G` shortcut, you create "
"a quick clipping group. That is, you group the layers, and a ‘mask layer’ "
"set with alpha inheritance is added on top."
msgstr ""
"アルファ継承を簡単に設定できるようになりました。レイヤーまたはレイヤーセット"
"を選択し、 :kbd:`Ctrl + Shift + G` ショートカットを押すと、 クイッククリッピ"
"ンググループ (quick clipping group) が作成されます。つまり、レイヤーをグルー"
"プ化して、アルファ継承を設定した 「マスクレイヤー」が一番上に追加されます。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:76
msgid ""
"The fact that alpha inheritance can use the composited transparency from a "
"combination of layers means that you can have a layer with the erase-"
"blending mode in between, and have that affect the area that the layer above "
"is clipped to. Above, the lower image is exactly the same as the upper one, "
"except with the erase-layer hidden. Filters can also affect the alpha "
"inheritance:"
msgstr ""
"アルファ継承では、レイヤーの組み合わせから合成された透明度を使用できるため、"
"レイヤー間に消去-合成モード (erase-blending mode) を持つレイヤーを作成し、そ"
"のレイヤーがクリップされる領域に影響を与えることができます。上記では、下の画"
"像は上の画像とまったく同じですが、消去レイヤー (erase-layer) が隠れています。"
"フィルタはアルファ継承にも影響します。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:83
msgid ""
"Above, the blur filter layer gives different results when in different "
"places, due to different parts being blurred."
msgstr ""
"上の例では、ブラーフィルタレイヤーが異なる場所にある場合、異なる部分がブラー"
"されるため、異なる結果になっています。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Composition_animation.gif\n"
"   :alt: Animation showing that groups are composed before the rest of "
"composition takes place."
msgstr ""
".. image:: images/clipping-masks/Composition_animation.gif\n"
"   :alt: 残りの合成が実行される前にグループが構成されることを示すアニメーショ"
"ン。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/layers/Layer-composite.png\n"
"   :alt: An image showing the way layers composite in Krita."
msgstr ""
".. image:: images/layers/Layer-composite.png\n"
"   :alt: Krita でのレイヤーの合成方法を示す画像。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/layers/Krita-tutorial2-I.1-2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/layers/Krita-tutorial2-I.1-2.png\n"
"   :alt: アルファ継承の動作とレイヤーへの影響を示す画像。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: An image with line art and a layer for each flat of color."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: 線画の画像と、各カラーフラットのレイヤー。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: アルファ継承の動作とレイヤーへの影響を示す画像。"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: Clipping mask step 3."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: クリッピングマスク ステップ3"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: Clipping mask step 4."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: クリッピングマスク ステップ4"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: Clipping mask step 5."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: クリッピングマスク ステップ5"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: Clipping mask step 6."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: クリッピングマスク ステップ6"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: Clipping mask step 7."
msgstr ""
".. image:: images/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: クリッピングマスク ステップ7"

#. +> trunk5
#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: Filter layers and alpha inheritance."
msgstr ""
".. image:: images/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: フィルタレイヤーとアルファ継承"
