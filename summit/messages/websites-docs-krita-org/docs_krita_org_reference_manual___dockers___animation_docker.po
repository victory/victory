# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-24 12:15+0200\n"
"PO-Revision-Date: 2021-05-08 21:13-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:1
msgid "Overview of the animation docker."
msgstr "アニメーションドッカーの概要。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:7
msgid ""
"As of Krita 5.0, the features of the Animation Docker have been moved to "
"the :ref:`timeline_docker`."
msgstr ""

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Animation"
msgstr "アニメーション"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Animation Playback"
msgstr "アニメーション再生"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Play/Pause"
msgstr "再生/一時停止"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Framerate"
msgstr "フレームレート"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "FPS"
msgstr ""

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Speed"
msgstr "速度"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:20
msgid "Animation Docker"
msgstr "アニメーションドッカー"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:23
msgid ".. image:: images/dockers/Animation_docker.png"
msgstr ""

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:24
msgid ""
"To have a playback of the animation, you need to use the animation docker."
msgstr ""
"アニメーションの再生をするには、アニメーションドッカーを使う必要があります。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:26
msgid ""
"The first big box represents the current Frame. The frames are counted with "
"programmer's counting so they start at 0."
msgstr ""
"一つ目の大きなボックスが現在のフレームを示しています。フレームはプログラマー"
"の方法で数えられるので0から始まります。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:28
msgid ""
"Then there are two boxes for you to change the playback range here. So, if "
"you want to do a 10 frame animation, set the end to 10, and then Krita will "
"cycle through the frames 0 to 10."
msgstr ""
"それから再生する範囲を決める二つのボックスがあります。なので10フレームのアニ"
"メーションにしたいなら、終了を10に設定すれば Krita は0から10のフレームで繰り"
"返します。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:30
msgid ""
"The bar in the middle is filled with playback options, and each of these can "
"also be hot-keyed. The difference between a keyframe and a normal frame in "
"this case is that a normal frame is empty, while a keyframe is filled."
msgstr ""
"中央のバーは再生オプションになっていて、それぞれのボタンはホットキーにも割り"
"当てることができます。ここでのキーフレームと通常のフレームの違いは通常のフ"
"レームは空白であるのに対し、キーフレームでは埋められています。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:32
msgid ""
"Then, there's buttons for adding, copying and removing frames. More "
"interesting is the next row:"
msgstr ""
"それからフレームを追加、コピーと削除するためのボタンがあります。もっと面白い"
"ものは次の行にあります。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:34
msgid "Onion Skin"
msgstr "オニオンスキン"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:35
msgid "Opens the :ref:`onion_skin_docker` if it wasn't open before."
msgstr ":ref:`onion_skin_docker` が開かれていないなら開きます。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:36
msgid "Auto Frame Mode"
msgstr "自動フレームモード"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:37
msgid ""
"Will make a frame out of any empty frame you are working on. Currently "
"automatically copies the previous frame."
msgstr ""
"作業中の空のフレームから新しいフレームを作成します。現在は自動的に前のフレー"
"ムをコピーします。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:39
msgid "Drop frames"
msgstr "フレーム落ちを有効"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:39
msgid ""
"This'll drop frames if your computer isn't fast enough to show all frames at "
"once. This process is automatic, but the icon will become red if it's forced "
"to do this."
msgstr ""
"全フレームを表示するにはコンピュータの処理速度が十分でない場合にフレームを落"
"とします。この処理は自動ですが、しなければならなくなった時にアイコンが赤色に"
"なります。"

#. +> trunk5
#: ../../reference_manual/dockers/animation_docker.rst:41
msgid ""
"You can also set the speedup of the playback, which is different from the "
"framerate."
msgstr "またフレームレートとは関係なしに、再生の速度を上げることもできます。"
