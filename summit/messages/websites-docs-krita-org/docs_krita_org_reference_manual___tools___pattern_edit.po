# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-24 12:16+0200\n"
"PO-Revision-Date: 2022-02-06 17:07-0800\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5
#: ../../<rst_epilog>:20
msgid ""
".. image:: images/icons/pattern_tool.svg\n"
"   :alt: toolpatternedit"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:1
msgid "Krita's vector pattern editing tool reference."
msgstr "Krita ベクターパターン編集ツールの説明。"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Tools"
msgstr "ツール"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:11
msgid "Pattern"
msgstr "パターン"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:16
msgid "Pattern Editing Tool"
msgstr "パターン編集ツール"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:18
msgid "|toolpatternedit|"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:22
msgid ""
"The pattern editing tool has been removed in 4.0, currently there's no way "
"to edit pattern fills for vectors."
msgstr ""
"このパターン編集ツールは4.0で削除されました。現在ベクターのパターンフィルを編"
"集する方法はありません。"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:24
msgid ""
"The Pattern editing tool works on Vector Shapes that use a Pattern fill. On "
"these shapes, the Pattern Editing Tool allows you to change the size, ratio "
"and origin of a pattern."
msgstr ""
"パターン編集ツールはパターン塗りつぶしを使用するベクターシェイプに使用しま"
"す。そういったシェイプでパターン元の縦横比やサイズを編集することができます。"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:27
msgid "On Canvas-editing"
msgstr "キャンバス上での操作"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:29
msgid ""
"You can change the origin by click dragging the upper node, this is only "
"possible in Tiled mode."
msgstr ""
"タイルモードでのみ可能で、上のノードをクリックすることでパターンの起点を変更"
"する事ができます。"

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:31
msgid ""
"You can change the size and ratio by click-dragging the lower node. There's "
"no way to constrain the ratio in on-canvas editing, this is only possible in "
"Original and Tiled mode."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:34
msgid "Tool Options"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:36
msgid "There are several tool options with this tool, for fine-tuning:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:38
msgid "First there are the Pattern options."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:41
msgid "This can be set to:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:43
msgid "Original:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:44
msgid "This will only show one, unstretched, copy of the pattern."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:45
msgid "Tiled (Default):"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:46
msgid "This will let the pattern appear tiled in the x and y direction."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Repeat:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "Stretch:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:48
msgid "This will stretch the Pattern image to the shape."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:51
msgid "Pattern origin. This can be set to:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:53
msgid "Top-left"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:54
msgid "Top"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:55
msgid "Top-right"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:56
msgid "Left"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:57
msgid "Center"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:58
msgid "Right"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:59
msgid "Bottom-left"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:60
msgid "Bottom"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Reference point:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:61
msgid "Bottom-right."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:64
msgid "For extra tweaking, set in percentages."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:66
msgid "X:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:67
msgid "Offset in the X coordinate, so horizontally."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Reference Point Offset:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Y:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:69
msgid "Offset in the Y coordinate, so vertically."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:71
msgid "Tile Offset:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:72
msgid "The tile offset if the pattern is tiled."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:74
msgid "Fine Tune the resizing of the pattern."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:76
msgid "W:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:77
msgid "The width, in pixels."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "Pattern Size:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "H:"
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:79
msgid "The height, in pixels."
msgstr ""

#. +> trunk5
#: ../../reference_manual/tools/pattern_edit.rst:81
msgid ""
"And then there's :guilabel:`Patterns`, which is a mini pattern docker, and "
"where you can pick the pattern used for the fill."
msgstr ""
