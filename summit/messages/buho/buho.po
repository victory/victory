msgid ""
msgstr ""
"Project-Id-Version: buho\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-18 09:24+0200\n"
"PO-Revision-Date: 2020-11-12 20:20-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5
#: src/main.cpp:54 src/main.qml:16
#, kde-format
msgid "Buho"
msgstr ""

#. +> trunk5
#: src/main.cpp:54
#, kde-format
msgid "Create and organize your notes."
msgstr ""

#. +> trunk5
#: src/main.cpp:54
#, kde-format
msgid "© 2019-%1 Maui Development Team"
msgstr ""

#. +> trunk5
#: src/main.cpp:55
#, kde-format
msgid "Camilo Higuita"
msgstr ""

#. +> trunk5
#: src/main.cpp:55
#, kde-format
msgid "Developer"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:71
#, kde-format
msgid "Remove notes"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:72
#, kde-format
msgid "Are you sure you want to delete the selected notes?"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:102
#, kde-format
msgid "No notes!"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:103
#, kde-format
msgid "You can quickly create a new note"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:107
#, kde-format
msgid "New note"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:129
#, kde-format
msgid "Search "
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:129
#, kde-format
msgid "notes"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:143
#, kde-format
msgid "Settings"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:150
#, kde-format
msgid "About"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:281
#, kde-format
msgid "Favorite"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:294 src/views/notes/NotesView.qml:540
#, kde-format
msgid "Share"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:300 src/views/notes/NotesView.qml:520
#, kde-format
msgid "Export"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:306 src/widgets/NewNoteDialog.qml:176
#, kde-format
msgid "Delete"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:509
#, kde-format
msgid "UnFav"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:509 src/widgets/SettingsDialog.qml:164
#, kde-format
msgid "Fav"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:530
#, kde-format
msgid "Copy"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:550
#, kde-format
msgid "Select"
msgstr ""

#. +> trunk5
#: src/views/notes/NotesView.qml:569
#, kde-format
msgid "Remove"
msgstr ""

#. +> trunk5
#: src/widgets/CardDelegate.qml:78
#, kde-format
msgid "Empty"
msgstr ""

#. +> trunk5
#: src/widgets/CardDelegate.qml:79
#, kde-format
msgid "Edit this note"
msgstr ""

#. +> trunk5
#: src/widgets/NewNoteDialog.qml:30
#, kde-format
msgid ""
"Title\n"
"Body"
msgstr ""

#. +> trunk5
#: src/widgets/NewNoteDialog.qml:54
#, kde-format
msgid "Note saved"
msgstr ""

#. +> trunk5
#: src/widgets/NewNoteDialog.qml:166
#, kde-format
msgid "Find and Replace"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:13
#, kde-format
msgid "Editor"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:14
#, kde-format
msgid "Configure the editor behaviour."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:18
#, kde-format
msgid "Auto Save"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:19
#, kde-format
msgid "Auto saves your file every few seconds"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:30
#, kde-format
msgid "Auto Reload"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:31
#, kde-format
msgid "Auto reload the text on external changes."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:42
#, kde-format
msgid "Line Numbers"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:43
#, kde-format
msgid "Display the line numbers on the left side."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:56
#, kde-format
msgid "Dark Mode"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:57
#, kde-format
msgid "Switch between light and dark colorscheme"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:74
#, kde-format
msgid "Fonts"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:75
#, kde-format
msgid "Configure the global editor font family and size"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:79
#, kde-format
msgid "Family"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:92
#, kde-format
msgid "Size"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:105
#, kde-format
msgid "Syncing"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:106
#, kde-format
msgid "Configure the syncing of notes and books."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:110
#, kde-format
msgid "Auto sync"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:111
#, kde-format
msgid "Sync notes and books on start up"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:124
#, kde-format
msgid "Sorting"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:125
#, kde-format
msgid "Sorting order and behavior."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:129
#, kde-format
msgid "Sorting by"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:130
#, kde-format
msgid "Change the sorting key."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:152
#, kde-format
msgid "Title"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:158
#, kde-format
msgid "Date"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:172
#, kde-format
msgid "Sort order"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:173
#, kde-format
msgid "Change the sorting order."
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:194
#, kde-format
msgid "Ascending"
msgstr ""

#. +> trunk5
#: src/widgets/SettingsDialog.qml:201
#, kde-format
msgid "Descending"
msgstr ""
