# translation of amarokpkg.po to Japanese
# Daniel E. Moctezuma <democtezuma@gmail.com>, 2010.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2011.
# Yasuhiko Kamata <belphegor@belbel.or.jp>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: amarokpkg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-11-18 08:41+0100\n"
"PO-Revision-Date: 2012-02-06 10:45+0900\n"
"Last-Translator: Yasuhiko Kamata <belphegor@belbel.or.jp>\n"
"Language-Team: Japanese <opensuse-ja@opensuse.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: KBabel 1.11.4\n"

#. +> trunk5
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Daniel E. Moctezuma"

#. +> trunk5
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "democtezuma@gmail.com"

#. +> trunk5
#: amarokpkg.cpp:84 amarokpkg.cpp:97
#, kde-format
msgid "Amarok Applet Manager"
msgstr "Amarok アプレットマネージャ"

#. +> trunk5
#: amarokpkg.cpp:86
#, kde-format
msgid "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"
msgstr "C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"

#. +> trunk5
#: amarokpkg.cpp:87
#, kde-format
msgid "Aaron Seigo"
msgstr "Aaron Seigo"

#. +> trunk5
#: amarokpkg.cpp:88
#, kde-format
msgid "Original author"
msgstr "原作者"

#. +> trunk5
#: amarokpkg.cpp:90
#, kde-format
msgid "Leo Franchi"
msgstr "Leo Franchi"

#. +> trunk5
#: amarokpkg.cpp:91
#, kde-format
msgid "Developer"
msgstr "開発者"

#. +> trunk5
#: amarokpkg.cpp:108
#, kde-format
msgid "For install or remove, operates on applets installed for all users."
msgstr ""
"インストールまたは削除を行なうには、すべてのユーザ向けにインストールされたア"
"プレットで操作してください。"

#. +> trunk5
#: amarokpkg.cpp:110
#, kde-format
msgctxt "Do not translate <path>"
msgid "Install the applet at <path>"
msgstr "<path> にあるアプレットをインストール"

#. +> trunk5
#: amarokpkg.cpp:112
#, kde-format
msgctxt "Do not translate <path>"
msgid "Upgrade the applet at <path>"
msgstr "<path> にあるアプレットをアップグレード"

#. +> trunk5
#: amarokpkg.cpp:114
#, fuzzy, kde-format
#| msgid "List installed applets"
msgid "Most installed applets"
msgstr "インストールされているアプレットの一覧表示"

#. +> trunk5
#: amarokpkg.cpp:116
#, kde-format
msgctxt "Do not translate <name>"
msgid "Remove the applet named <name>"
msgstr "<name> という名前のアプレットを削除"

#. +> trunk5
#: amarokpkg.cpp:118
#, kde-format
msgid ""
"Absolute path to the package root. If not supplied, then the standard data "
"directories for this KDE session will be searched instead."
msgstr ""
"パッケージルートに対する絶対パスを指定します。何も指定しない場合は、この KDE "
"セッション向けの標準データディレクトリを検索します。"

#. +> trunk5
#: amarokpkg.cpp:176
#, kde-format
msgid "Successfully removed %1"
msgstr "%1 の削除に成功しました。"

#. +> trunk5
#: amarokpkg.cpp:178
#, kde-format
msgid "Removal of %1 failed."
msgstr "%1 の削除に失敗しました。"

#. +> trunk5
#: amarokpkg.cpp:183
#, kde-format
msgid "Plugin %1 is not installed."
msgstr "%1 のプラグインはインストールしていません。"

#. +> trunk5
#: amarokpkg.cpp:188
#, kde-format
msgid "Successfully installed %1"
msgstr "%1 のインストールに成功しました。"

#. +> trunk5
#: amarokpkg.cpp:191
#, kde-format
msgid "Installation of %1 failed."
msgstr "%1 のインストールに失敗しました。"

#. +> trunk5
#: amarokpkg.cpp:199
#, kde-format
msgctxt ""
"No option was given, this is the error message telling the user he needs at "
"least one, do not translate install, remove, upgrade nor list"
msgid "One of install, remove, upgrade or list is required."
msgstr "install/remove/upgrade/list のいずれかを指定する必要があります。"

#~ msgid "Install, list, remove Amarok applets"
#~ msgstr "Amarok アプレットのインストール、一覧表示、削除"

#~ msgid "List installed applets"
#~ msgstr "インストールされているアプレットの一覧表示"
