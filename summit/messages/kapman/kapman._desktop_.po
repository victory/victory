# Translation of desktop_kdegames into Japanese.
# Taiki Komoda <kom@kde.gr.jp>, 2002.
# Takuro Ashie <ashie@homa.ne.jp>, 2004.
# Kurose Shushi <md81@bird.email.ne.jp>, 2004.
# AWASHIRO Ikuya <ikuya@oooug.jp>, 2004.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2006.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2007, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdegames\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-23 09:56+0100\n"
"PO-Revision-Date: 2009-01-08 23:33+0900\n"
"Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5 stable5
#: src/org.kde.kapman.desktop:4
msgctxt "Name"
msgid "Kapman"
msgstr "Kapman"

#. +> trunk5 stable5
#: src/org.kde.kapman.desktop:58
msgctxt "GenericName"
msgid "Pac-Man Clone"
msgstr "パックマンのクローン"

#. +> trunk5 stable5
#: src/org.kde.kapman.desktop:107
msgctxt "Comment"
msgid "Eat pills escaping ghosts"
msgstr "モンスターを避けてドットを食べるゲーム"

#. +> trunk5 stable5
#: themes/invisible.desktop:2
msgctxt "Name"
msgid "Invisible"
msgstr "暗闇"

#. +> trunk5 stable5
#: themes/invisible.desktop:54
msgctxt "Description"
msgid ""
"Getting bored with Kapman? More than 100,000 points over level 20? Next "
"step: the invisible maze!"
msgstr ""
"Kapman に飽きてきたって？レベル 20 をクリアして 100,000 ポイント獲得？次は暗"
"闇の迷路に挑戦!"

#. +> trunk5 stable5
#: themes/matches.desktop:2
msgctxt "Name"
msgid "Matches"
msgstr "マッチ棒"

#. +> trunk5 stable5
#: themes/matches.desktop:54
msgctxt "Description"
msgid "A matches drawn maze"
msgstr "マッチ棒でできた迷路"

#. +> trunk5 stable5
#: themes/mountain.desktop:2
msgctxt "Name"
msgid "Mountain Adventure"
msgstr ""

#. +> trunk5 stable5
#: themes/mountain.desktop:50
msgctxt "Description"
msgid "Default"
msgstr "標準"

#. +> trunk5 stable5
#: themes/mummies_crypt.desktop:2
msgctxt "Name"
msgid "Mummies Crypt"
msgstr ""

#. +> trunk5 stable5
#: themes/mummies_crypt.desktop:49
msgctxt "Description"
msgid "Avoid the mummies at all costs!"
msgstr ""

#. +> trunk5 stable5
#: themes/retro.desktop:2
msgctxt "Name"
msgid "Retro"
msgstr "レトロ"

#. +> trunk5 stable5
#: themes/retro.desktop:54
msgctxt "Description"
msgid "The old game theme revisited"
msgstr "昔のテーマのリメイク"
