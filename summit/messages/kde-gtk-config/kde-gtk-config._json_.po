msgid ""
msgstr ""
"Project-Id-Version: json files\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-20 15:47+0200\n"
"PO-Revision-Date: 2019-11-21 14:06-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5 stable5 plasma5lts
#: kded/gtkconfig.json
msgctxt "Name"
msgid "GNOME/GTK Settings Synchronization Service"
msgstr ""

#. +> trunk5 stable5 plasma5lts
#: kded/gtkconfig.json
msgctxt "Description"
msgid "Automatically applies settings to GNOME/GTK applications"
msgstr ""
