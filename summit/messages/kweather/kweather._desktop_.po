msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-22 10:44+0100\n"
"PO-Revision-Date: 2020-06-01 21:32-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5
#: org.kde.kweather.desktop:9
msgctxt "Name"
msgid "Weather"
msgstr ""

#. +> trunk5
#: org.kde.kweather.desktop:40
msgctxt "Comment"
msgid "View real-time weather forecasts and other information"
msgstr ""

#. +> trunk5
#: src/plasmoid/package/metadata.desktop:8
msgctxt "Name"
msgid "KWeather_1x4"
msgstr ""

#. +> trunk5
#: src/plasmoid/package/metadata.desktop:37
msgctxt "Comment"
msgid "KWeather_1x4 widget"
msgstr ""
