# Translation of kate-ctags-plugin into Japanese.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2008.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kate-ctags-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-25 10:48+0100\n"
"PO-Revision-Date: 2011-04-21 22:21-0700\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Fumiaki Okushi"

#. +> trunk5 stable5
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "okushi@kde.gr.jp"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#. +> trunk5 stable5
#: CTagsGlobalConfig.ui:9
#, kde-format
msgid "Session-global index targets"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, addButton)
#. +> trunk5 stable5
#: CTagsGlobalConfig.ui:33 kate_ctags.ui:65
#, kde-format
msgid "Add"
msgstr "追加"

#. i18n: ectx: property (text), widget (QPushButton, delButton)
#. +> trunk5 stable5
#: CTagsGlobalConfig.ui:40 kate_ctags.ui:72
#, kde-format
msgid "Remove"
msgstr "削除"

#. i18n: ectx: property (text), widget (QPushButton, updateDB)
#. +> trunk5 stable5
#: CTagsGlobalConfig.ui:54
#, kde-format
msgid "Update"
msgstr "更新"

#. i18n: ectx: property (text), widget (QLabel, cmdLabel)
#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#. +> trunk5 stable5
#: CTagsGlobalConfig.ui:66 kate_ctags.ui:119
#, kde-format
msgid "CTags command"
msgstr "CTags コマンド"

#. +> trunk5 stable5
#: ctagskinds.cpp:23
msgctxt "Tag Type"
msgid "define"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:24 ctagskinds.cpp:66
msgctxt "Tag Type"
msgid "label"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:25 ctagskinds.cpp:39 ctagskinds.cpp:85
msgctxt "Tag Type"
msgid "macro"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:28 ctagskinds.cpp:30 ctagskinds.cpp:41 ctagskinds.cpp:63
#: ctagskinds.cpp:83 ctagskinds.cpp:87 ctagskinds.cpp:91 ctagskinds.cpp:93
#: ctagskinds.cpp:98 ctagskinds.cpp:102 ctagskinds.cpp:104 ctagskinds.cpp:106
#: ctagskinds.cpp:110
msgctxt "Tag Type"
msgid "function"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:28 ctagskinds.cpp:71 ctagskinds.cpp:89 ctagskinds.cpp:95
msgctxt "Tag Type"
msgid "subroutine"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:32
msgctxt "Tag Type"
msgid "fragment definition"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:33
msgctxt "Tag Type"
msgid "any pattern"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:34
msgctxt "Tag Type"
msgid "slot"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:35
msgctxt "Tag Type"
msgid "pattern"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:38 ctagskinds.cpp:55 ctagskinds.cpp:76 ctagskinds.cpp:91
#: ctagskinds.cpp:93 ctagskinds.cpp:97
msgctxt "Tag Type"
msgid "class"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:40
msgctxt "Tag Type"
msgid "enumerator"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:42
msgctxt "Tag Type"
msgid "enumeration"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:43
msgctxt "Tag Type"
msgid "member"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:44 ctagskinds.cpp:106
msgctxt "Tag Type"
msgid "namespace"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:45
msgctxt "Tag Type"
msgid "prototype"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:46
msgctxt "Tag Type"
msgid "struct"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:47
msgctxt "Tag Type"
msgid "typedef"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:48
msgctxt "Tag Type"
msgid "union"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:49 ctagskinds.cpp:73
msgctxt "Tag Type"
msgid "variable"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:50
msgctxt "Tag Type"
msgid "external variable"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:53
msgctxt "Tag Type"
msgid "paragraph"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:56
msgctxt "Tag Type"
msgid "feature"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:57
msgctxt "Tag Type"
msgid "local entity"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:60
msgctxt "Tag Type"
msgid "block"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:61
msgctxt "Tag Type"
msgid "common"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:62
msgctxt "Tag Type"
msgid "entry"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:64 ctagskinds.cpp:78
msgctxt "Tag Type"
msgid "interface"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:65
msgctxt "Tag Type"
msgid "type component"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:67
msgctxt "Tag Type"
msgid "local"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:68
msgctxt "Tag Type"
msgid "module"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:69
msgctxt "Tag Type"
msgid "namelist"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:70
msgctxt "Tag Type"
msgid "program"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:72
msgctxt "Tag Type"
msgid "type"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:77
msgctxt "Tag Type"
msgid "field"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:79
msgctxt "Tag Type"
msgid "method"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:80
msgctxt "Tag Type"
msgid "package"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:87 ctagskinds.cpp:108
msgctxt "Tag Type"
msgid "procedure"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:99
msgctxt "Tag Type"
msgid "mixin"
msgstr ""

#. +> trunk5 stable5
#: ctagskinds.cpp:102
msgctxt "Tag Type"
msgid "set"
msgstr ""

#. +> trunk5 stable5
#: gotosymbolmodel.cpp:66
#, fuzzy, kde-format
#| msgid "The CTags executable crashed."
msgid "CTags executable not found."
msgstr "CTags プログラムがクラッシュしました。"

#. +> trunk5 stable5
#: gotosymbolmodel.cpp:79
#, fuzzy, kde-format
#| msgid "The CTags executable crashed."
msgid "CTags executable failed to execute."
msgstr "CTags プログラムがクラッシュしました。"

#. +> trunk5 stable5
#: gotosymbolmodel.cpp:147
#, fuzzy, kde-format
#| msgid "CTags database file"
msgid "CTags was unable to parse this file."
msgstr "CTags データベースファイル"

#. +> trunk5 stable5
#: gotosymbolwidget.cpp:246
#, kde-format
msgid ""
"Tags file not found. Please generate one manually or using the CTags plugin"
msgstr ""

#. +> trunk5 stable5
#: gotosymbolwidget.cpp:315
#, kde-format
msgid "File for '%1' not found."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, widget)
#. +> trunk5 stable5
#: kate_ctags.ui:13
#, kde-format
msgid "Lookup"
msgstr "検索"

#. i18n: ectx: property (text), widget (QPushButton, updateButton)
#. i18n: ectx: property (text), widget (QPushButton, updateButton2)
#. +> trunk5 stable5
#: kate_ctags.ui:26 kate_ctags.ui:92
#, kde-format
msgid "Update Index"
msgstr "インデックスを更新"

#. i18n: ectx: property (text), widget (QTreeWidget, tagTreeWidget)
#. +> trunk5 stable5
#: kate_ctags.ui:37
#, kde-format
msgid "Tag"
msgstr "タグ"

#. i18n: ectx: property (text), widget (QTreeWidget, tagTreeWidget)
#. +> trunk5 stable5
#: kate_ctags.ui:42
#, kde-format
msgid "Type"
msgstr "タイプ"

#. i18n: ectx: property (text), widget (QTreeWidget, tagTreeWidget)
#. +> trunk5 stable5
#: kate_ctags.ui:47
#, kde-format
msgid "File"
msgstr "ファイル"

#. i18n: ectx: attribute (title), widget (QWidget, targets)
#. +> trunk5 stable5
#: kate_ctags.ui:56
#, kde-format
msgid "Index Targets"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, database)
#. +> trunk5 stable5
#: kate_ctags.ui:100
#, kde-format
msgid "Database"
msgstr "データベース"

#. i18n: ectx: property (text), widget (QLabel, fileLabel)
#. +> trunk5 stable5
#: kate_ctags.ui:109
#, kde-format
msgid "CTags database file"
msgstr "CTags データベースファイル"

#. i18n: ectx: property (toolTip), widget (QToolButton, resetCMD)
#. +> trunk5 stable5
#: kate_ctags.ui:152
#, kde-format
msgid "Revert to the default command"
msgstr ""

#. i18n: ectx: property (text), widget (QToolButton, resetCMD)
#. +> trunk5 stable5
#: kate_ctags.ui:155
#, kde-format
msgid "..."
msgstr ""

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:79 kate_ctags_view.cpp:108
#, kde-format
msgid "Add a directory to index."
msgstr "インデックスにディレクトリを追加。"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:82 kate_ctags_view.cpp:111
#, kde-format
msgid "Remove a directory."
msgstr "ディレクトリを削除。"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:85
#, kde-format
msgid "(Re-)generate the common CTags database."
msgstr "CTags データベースファイルを作成し直します。"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:103 kate_ctags_view.cpp:52 kate_ctags_view.cpp:95
#, kde-format
msgid "CTags"
msgstr "CTags"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:109
#, kde-format
msgid "CTags Settings"
msgstr "CTags 設定"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:231 kate_ctags_view.cpp:551
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "%1 を実行できませんでした。終了ステータス = %2"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:242 kate_ctags_view.cpp:563
#, kde-format
msgid "The CTags executable crashed."
msgstr "CTags プログラムがクラッシュしました。"

#. +> trunk5 stable5
#: kate_ctags_plugin.cpp:244
#, kde-format
msgid "The CTags command exited with code %1"
msgstr "CTags コマンドは終了コード %1 で終了しました"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:45
#, fuzzy, kde-format
#| msgid "CTags"
msgid "Kate CTags"
msgstr "CTags"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:56
#, kde-format
msgid "Jump back one step"
msgstr "1 ステップ戻ります"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:60
#, kde-format
msgid "Go to Declaration"
msgstr "宣言に移動"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:64
#, kde-format
msgid "Go to Definition"
msgstr "定義に移動"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:68
#, kde-format
msgid "Lookup Current Text"
msgstr "現在のテキストを検索"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:72
#, kde-format
msgid "Configure ..."
msgstr ""

#. +> trunk5 stable5
#: kate_ctags_view.cpp:87
#, fuzzy, kde-format
#| msgid "CTags Plugin"
msgctxt "@title:window"
msgid "Configure CTags Plugin"
msgstr "CTags プラグイン"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:98 kate_ctags_view.cpp:187
#, kde-format
msgid "Go to Declaration: %1"
msgstr "宣言に移動: %1"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:99 kate_ctags_view.cpp:188
#, kde-format
msgid "Go to Definition: %1"
msgstr "定義に移動: %1"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:100 kate_ctags_view.cpp:189
#, kde-format
msgid "Lookup: %1"
msgstr "検索: %1"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:114 kate_ctags_view.cpp:117
#, kde-format
msgid "(Re-)generate the session specific CTags database."
msgstr "セッション依存の CTags データベースファイルを作成し直します。"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:122
#, kde-format
msgid "Select new or existing database file."
msgstr "新規か既存の CTags データベースを選択します。"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:138
#, kde-format
msgid "Go To Local Symbol"
msgstr ""

#. +> trunk5 stable5
#: kate_ctags_view.cpp:143
#, kde-format
msgid "Go To Global Symbol"
msgstr ""

#. +> trunk5 stable5
#: kate_ctags_view.cpp:315 kate_ctags_view.cpp:347
#, kde-format
msgid "No hits found"
msgstr "マッチするものがありませんでした"

#. +> trunk5 stable5
#: kate_ctags_view.cpp:540
#, kde-format
msgid "No folders or files to index"
msgstr ""

#. +> trunk5 stable5
#: kate_ctags_view.cpp:565
#, fuzzy, kde-format
#| msgid "The CTags program exited with code %1"
msgid "The CTags program exited with code %1: %2"
msgstr "CTags プログラムは終了コード %1 で終了しました"

#~ msgid "The CTags program exited with code %1"
#~ msgstr "CTags プログラムは終了コード %1 で終了しました"

#~ msgid "Lookup:"
#~ msgstr "検索:"

#~ msgid "Tags"
#~ msgstr "タグ"

#~ msgid "Settings"
#~ msgstr "設定"

#~ msgid "CTags command:"
#~ msgstr "CTags コマンド:"

#~ msgid "CTags database file:"
#~ msgstr "CTags データベースファイル:"

#~ msgid "Load a CTags database file."
#~ msgstr "CTags データベースファイルを読み込みます。"

#~ msgid "Create a CTags database file."
#~ msgstr "CTags データベースファイルを作成します。"

#~ msgid "Re-generate the CTags database file."
#~ msgstr "CTags データベースファイルを作成し直します。"

#~ msgctxt "Button text for creating a new CTags database file."
#~ msgid "Create New"
#~ msgstr "新規作成"

#~ msgid "Select a location and create a new CTags database."
#~ msgstr "場所を選択して新しい CTags データベースを作成します。"

#~ msgctxt "Button text for loading a CTags database file"
#~ msgid "Load"
#~ msgstr "読み込み"

#~ msgid "Select an existing CTags database."
#~ msgstr "既存の CTags データベースを選択します。"

#~ msgid "No CTags database is loaded."
#~ msgstr "CTags データベースが読み込まれていません。"

#~ msgid "Multiple hits found."
#~ msgstr "マッチするものが複数見つかりました。"

#~ msgid "CTags Database Location"
#~ msgstr "CTags データベースの場所"
