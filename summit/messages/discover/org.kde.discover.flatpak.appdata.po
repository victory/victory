msgid ""
msgstr ""
"Project-Id-Version: org.kde.discover.flatpak.appdata\n"
"POT-Creation-Date: 2018-05-19 11:44+0200\n"
"PO-Revision-Date: 2018-01-11 20:55-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. (itstool) path: component/name
#. +> trunk5 stable5 plasma5lts
#: org.kde.discover.flatpak.appdata.xml:4
msgid "Flatpak backend"
msgstr ""

#. (itstool) path: component/summary
#. +> trunk5 stable5 plasma5lts
#: org.kde.discover.flatpak.appdata.xml:5
msgid "Integrates Flatpak applications into Discover"
msgstr ""

#. (itstool) path: component/developer_name
#. +> trunk5 stable5 plasma5lts
#: org.kde.discover.flatpak.appdata.xml:9
msgid "Aleix Pol Gonzalez"
msgstr ""
