msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-18 09:07+0100\n"
"PO-Revision-Date: 2019-05-31 10:58-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5
#: src/org.kde.ktimetracker.desktop:2
msgctxt "Name"
msgid "KTimeTracker"
msgstr ""

#. +> trunk5
#: src/org.kde.ktimetracker.desktop:30
msgctxt "GenericName"
msgid "Personal Time Tracker"
msgstr ""
