msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-21 09:52+0100\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:16
msgid "Project Menu"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:18
msgid "Contents"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:24
msgid ":ref:`Add Clip or Folder... <add_clip>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:26
msgid ":ref:`Add Color Clip... <add_color_clip>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:28
msgid ":ref:`Add Slideshow Clip <add_slideshow_clip>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:30
msgid ":ref:`Add Title Clip... <titles>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:32
msgid ":ref:`Add Template Title... <titles>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:34
msgid ":ref:`create_folder`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:36
msgid ":ref:`online_resources`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:38
msgid ":ref:`generators`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:40
msgid ":ref:`view_mode`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:42
msgid ":ref:`clean_project`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:44
msgid ":ref:`Render... <render>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:46
msgid ":ref:`extract_audio`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:48
msgid ":ref:`adjust_profile_to_current_clip`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:50
msgid ":ref:`Archive Project... <archiving>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:52
msgid ":ref:`Open Backup File... <open_backup_file>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:54
msgid ":ref:`Project Settings... <project_settings>`"
msgstr ""

#. +> trunk5
#: ../../user_interface/menu/project_menu.rst:57
msgid "Contents:"
msgstr ""
