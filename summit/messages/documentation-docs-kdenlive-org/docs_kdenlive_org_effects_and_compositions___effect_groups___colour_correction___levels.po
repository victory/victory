msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-11-18 11:08+0100\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:14
msgid "Levels"
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:16
msgid "Contents"
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:18
msgid ""
"This is the `Frei0r levels <https://www.mltframework.org/plugins/"
"FilterFrei0r-levels/>`_ MLT filter."
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/colour_correction/levels.rst:20
msgid "https://youtu.be/iMbohQnyFV4"
msgstr ""
