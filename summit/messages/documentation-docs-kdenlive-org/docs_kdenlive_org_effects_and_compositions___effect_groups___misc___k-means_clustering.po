msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-03 11:04+0100\n"
"PO-Revision-Date: 2021-11-20 18:29-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:13
msgid "K-Means Clustering"
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:15
msgid "Contents"
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:17
msgid ""
"This is the `Frei0r cluster <https://www.mltframework.org/plugins/"
"FilterFrei0r-cluster/>`_ MLT filter."
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:19
msgid "Clusters of a source image by color and spatial distance."
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:21
msgid "https://youtu.be/a3Yz2xJWmN8"
msgstr ""

#. +> trunk5
#: ../../effects_and_compositions/effect_groups/misc/k-means_clustering.rst:23
msgid "https://youtu.be/qwTD__a5oqo"
msgstr ""
