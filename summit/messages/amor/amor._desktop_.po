# translation of desktop_kdetoys.po to Japanese
# Taiki Komoda <kom@kde.gr.jp>, 2002.
# SATOH Satoru <ss@kde.gr.jp>, 2004.
# Kurose Shushi <md81@bird.email.ne.jp>, 2004.
# Kenshi Muto <kmuto@debian.org>, 2005.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2006, 2007.
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdetoys\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-11-04 11:23+0100\n"
"PO-Revision-Date: 2007-09-27 21:00+0900\n"
"Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.10.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: \n"
"X-Text-Markup: \n"

#. +> trunk5
#: data/billyrc:5
msgctxt "Description"
msgid "Little Billy"
msgstr "リトルビル"

#. +> trunk5
#: data/billyrc:78
msgctxt "About"
msgid "Static window sitter\\nGraphic from http://www.xbill.org/"
msgstr "動かないウィンドウ監視人\\nグラフィックは http://www.xbill.org/ より"

#. +> trunk5
#: data/blobrc:4
msgctxt "Description"
msgid "Multi-Talented Spot"
msgstr "マルチタレントスポット"

#. +> trunk5
#: data/blobrc:70
msgctxt "About"
msgid "By Martin R. Jones\\nJet pack, beam and fire animations by Mark Grant"
msgstr ""
"Martin R. Jones 作\\nジェットパック、ビーム、ファイアアニメーションは Mark "
"Grant 作"

#. +> trunk5
#: data/bonhommerc:8
msgctxt "Description"
msgid "Bonhomme"
msgstr "Bonhomme"

#. +> trunk5
#: data/bonhommerc:76 data/eyesrc:76
msgctxt "About"
msgid "By Jean-Claude Dumas"
msgstr "Jean-Claude Dumas 作"

#. +> trunk5
#: data/bsdrc:5
msgctxt "Description"
msgid "FreeBSD Mascot"
msgstr "FreeBSD マスコット"

#. +> trunk5
#: data/bsdrc:75 data/tuxrc:76
msgctxt "About"
msgid "Static window sitter"
msgstr "動かないウィンドウ監視人"

#. +> trunk5
#: data/eyesrc:6
msgctxt "Description"
msgid "Crazy Eyes"
msgstr "クレイジーアイ"

#. +> trunk5
#: data/ghostrc:4
msgctxt "Description"
msgid "Spooky Ghost"
msgstr "ゆうれい"

#. +> trunk5
#: data/ghostrc:74
msgctxt "About"
msgid "By Martin R. Jones\\nBased on an icon by the KDE artist team."
msgstr "Martin R. Jones 作\\nアイコンは KDE アーティストチーム作"

#. +> trunk5
#: data/nekokurorc:9
#, fuzzy
#| msgctxt "Description"
#| msgid "Neko"
msgctxt "Description"
msgid "Neko Kuro"
msgstr "Neko"

#. +> trunk5
#: data/nekokurorc:60
#, fuzzy
#| msgctxt "About"
#| msgid "Artwork from oneko by Masayuki Koba\\nAMOR'd by Chris Spiegel"
msgctxt "About"
msgid ""
"Artwork from oneko by Masayuki Koba\\nAMOR'd by Chris Spiegel\\nKuro (Black) "
"version by Bill Kendrick"
msgstr "oneko のグラフィック - Masayuki Koba 作\\nChris Spiegel が AMOR 化"

#. +> trunk5
#: data/nekorc:7
msgctxt "Description"
msgid "Neko"
msgstr "Neko"

#. +> trunk5
#: data/nekorc:79
msgctxt "About"
msgid "Artwork from oneko by Masayuki Koba\\nAMOR'd by Chris Spiegel"
msgstr "oneko のグラフィック - Masayuki Koba 作\\nChris Spiegel が AMOR 化"

#. +> trunk5
#: data/pingurc:4
msgctxt "Description"
msgid "Tux"
msgstr "Tux"

#. +> trunk5
#: data/pingurc:77
msgctxt "About"
msgid "By Frank Pieczynski\\nBased on graphics of the game \"pingus\"."
msgstr "Frank Pieczynski 作\\nゲーム “pingus” のグラフィックに基づく"

#. +> trunk5
#: data/taorc:4
msgctxt "Description"
msgid "Tao"
msgstr "Tao"

#. +> trunk5
#: data/taorc:77
msgctxt "About"
msgid ""
"By Daniel Pfeiffer <occitan@esperanto.org>\\nYin Yang symbol inspired by my "
"Tai Chi practice."
msgstr ""
"Daniel Pfeiffer <occitan@esperanto.org> 作\\n太極拳の練習から発想を得た陰陽シ"
"ンボル"

#. +> trunk5
#: data/tuxrc:5
msgctxt "Description"
msgid "Unanimated Tux"
msgstr "動かない Tux"

#. +> trunk5
#: data/wormrc:4
msgctxt "Description"
msgid "Little Worm"
msgstr "小さいむし"

#. +> trunk5
#: data/wormrc:77
msgctxt "About"
msgid "By Bartosz Trudnowski\\nMade for my wife"
msgstr "Bartosz Trudnowski 作\\n妻のために"

#. +> trunk5
#: src/org.kde.amor.desktop:2
msgctxt "Name"
msgid "AMOR"
msgstr "AMOR"

#. +> trunk5
#: src/org.kde.amor.desktop:78
msgctxt "GenericName"
msgid "On-Screen Creature"
msgstr "画面上の生物"
