msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.private.grouping\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-09-25 09:28+0200\n"
"PO-Revision-Date: 2021-10-16 18:52+0900\n"
"Last-Translator: R.Suga <21r.suga@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Poedit 2.4.2\n"

#. +> trunk5 stable5 plasma5lts
#: package/contents/ui/main.qml:137
#, kde-format
msgid "Drag applets here"
msgstr "ここにアプレットをドラッグ"
