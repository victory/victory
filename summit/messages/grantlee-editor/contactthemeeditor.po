msgid ""
msgstr ""
"Project-Id-Version: contactthemeeditor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-13 12:57+0100\n"
"PO-Revision-Date: 2013-08-19 00:13-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#. +> trunk5 stable5
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#. +> trunk5 stable5
#: contactconfigurationdialog.cpp:31
#, kde-format
msgctxt "@title:window"
msgid "Configure"
msgstr ""

#. +> trunk5 stable5
#: contactconfigurationdialog.cpp:41
#, kde-format
msgid "Default contact:"
msgstr ""

#. +> trunk5 stable5
#: contactconfigurationdialog.cpp:47
#, kde-format
msgid "General"
msgstr ""

#. +> trunk5 stable5
#: contactconfigurationdialog.cpp:50
#, kde-format
msgid "Default Template"
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:75
#, kde-format
msgid "Load Recent Theme..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:82
#, kde-format
msgid "Add Extra Page..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:87
#, kde-format
msgid "Upload theme..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:93
#, kde-format
msgid "New theme..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:96
#, kde-format
msgid "Open theme..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:98
#, kde-format
msgid "Save theme"
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:100
#, kde-format
msgid "Save theme as..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:106
#, kde-format
msgid "Install theme"
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:110
#, kde-format
msgid "Insert File..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:114
#, kde-format
msgid "Manage themes..."
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:118
#, kde-format
msgid "Update view"
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:186 contacteditormainwindow.cpp:318
#, kde-format
msgid "Select theme directory"
msgstr ""

#. +> trunk5 stable5
#: contacteditormainwindow.cpp:202
#, kde-format
msgid "Directory does not contain a theme file. We cannot load theme."
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:42
#, kde-format
msgid "Editor"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:54
#, kde-format
msgid "Desktop File"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:125
#, kde-format
msgid "Theme already exists. Do you want to overwrite it?"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:126
#, kde-format
msgid "Theme already exists"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:134
#, kde-format
msgid "Cannot create theme folder."
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:148
#, kde-format
msgid "Theme installed in \"%1\""
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:171 contacteditorpage.cpp:177
#, kde-format
msgid "We cannot add preview file in zip file"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:171 contacteditorpage.cpp:177
#, kde-format
msgid "Failed to add file."
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:190
#, kde-format
msgid "My favorite Kaddressbook theme"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:216
#, kde-format
msgid "Filename of extra page"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:216
#, kde-format
msgid "Filename:"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:274
#, kde-format
msgid "Do you want to save current project?"
msgstr ""

#. +> trunk5 stable5
#: contacteditorpage.cpp:275
#, kde-format
msgid "Save current project"
msgstr ""

#. +> trunk5 stable5
#: contactpreviewwidget.cpp:34
#, kde-format
msgid "Contact"
msgstr ""

#. +> trunk5 stable5
#: contactpreviewwidget.cpp:37
#, kde-format
msgid "Group"
msgstr ""

#. +> trunk5 stable5
#: contacttemplatewidget.cpp:34
#, kde-format
msgid "You can drag and drop element on editor to import template"
msgstr ""

#. i18n: ectx: Menu (edit)
#. +> trunk5 stable5
#: contactthemeeditorui.rc:16
#, kde-format
msgid "&Edit"
msgstr ""

#. i18n: ectx: Menu (Display)
#. +> trunk5 stable5
#: contactthemeeditorui.rc:22
#, kde-format
msgid "&Display"
msgstr ""

#. +> trunk5 stable5
#: editorpage.cpp:52
#, kde-format
msgid "Theme Templates:"
msgstr ""

#. +> trunk5 stable5
#: main.cpp:36 main.cpp:38
#, kde-format
msgid "Contact Theme Editor"
msgstr ""

#. +> trunk5 stable5
#: main.cpp:40
#, kde-format
msgid "Copyright © 2013-%1 contactthemeeditor authors"
msgstr ""

#. +> trunk5 stable5
#: main.cpp:41
#, kde-format
msgid "Laurent Montel"
msgstr ""

#. +> trunk5 stable5
#: main.cpp:41
#, kde-format
msgid "Maintainer"
msgstr ""
