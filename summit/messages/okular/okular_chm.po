# translation of okular_chm.po to Japanese
# This file is distributed under the same license as the kdegraphics package.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: okular_chm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-10 10:19+0100\n"
"PO-Revision-Date: 2007-07-18 23:00+0900\n"
"Last-Translator: Yukiko Bando <ybando@k6.dion.ne.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Yukiko Bando"

#. +> trunk5 stable5
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ybando@k6.dion.ne.jp"

#. +> trunk5 stable5
#: lib/ebook_epub.cpp:311
#, kde-format
msgid "Unsupported encoding"
msgstr ""

#. +> trunk5 stable5
#: lib/ebook_epub.cpp:311
#, kde-format
msgid ""
"The encoding of this ebook is not supported yet. Please open a bug in "
"https://bugs.kde.org for support to be added"
msgstr ""

#~ msgid "CHM Backend"
#~ msgstr "CHM バックエンド"

#~ msgid "A Microsoft Windows help file renderer"
#~ msgstr "Microsoft Windows ヘルプファイルのレンダラー"

#~ msgid ""
#~ "© 2005-2007 Piotr Szymański\n"
#~ "© 2008 Albert Astals Cid"
#~ msgstr ""
#~ "© 2005-2007 Piotr Szymański\n"
#~ "© 2008 Albert Astals Cid"

#~ msgid "Piotr Szymański"
#~ msgstr "Piotr Szymański"

#~ msgid "Albert Astals Cid"
#~ msgstr "Albert Astals Cid"

#~ msgid "© 2005-2007 Piotr Szymański"
#~ msgstr "© 2005-2007 Piotr Szymański"
