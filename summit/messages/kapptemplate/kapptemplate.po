# Translation of kapptemplate into Japanese.
# Yukiko Bando <ybando@k6.dion.ne.jp>, 2009.
# Fumiaki Okushi <okushi@kde.gr.jp>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kapptemplate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-04-02 10:29+0200\n"
"PO-Revision-Date: 2011-04-08 20:46-0700\n"
"Last-Translator: Fumiaki Okushi <okushi@kde.gr.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#. +> trunk5 stable5
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Yukiko Bando"

#. +> trunk5 stable5
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ybando@k6.dion.ne.jp"

#. +> trunk5 stable5
#: src/application/apptemplatesmodel.cpp:158
#, fuzzy, kde-format
#| msgid "Templates Projects"
msgctxt "@title:column"
msgid "Templates Projects"
msgstr "プロジェクトのテンプレート"

#. i18n: ectx: property (text), widget (QPushButton, getNewButton)
#. +> trunk5 stable5
#: src/application/choice.ui:37
#, kde-format
msgid "Get More Templates"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, installButton)
#. +> trunk5 stable5
#: src/application/choice.ui:50
#, kde-format
msgid "Install Template From File"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, nameLabel)
#. +> trunk5 stable5
#: src/application/choice.ui:111
#, kde-format
msgid "Project name:"
msgstr "プロジェクト名:"

#. +> trunk5 stable5
#: src/application/choicepage.cpp:34
#, kde-format
msgid "Choose your project template"
msgstr "プロジェクトのテンプレートを選択"

#. +> trunk5 stable5
#: src/application/choicepage.cpp:145
#, kde-format
msgid "No sample picture available."
msgstr ""

#. +> trunk5 stable5
#: src/application/choicepage.cpp:158
#, kde-format
msgid "Template description"
msgstr "テンプレートの説明"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:30
#, kde-format
msgid "Generating your project"
msgstr "プロジェクトの生成"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:46
#, kde-format
msgid "%1 cannot be created."
msgstr "‘%1’ を作成できません。"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:66
#, fuzzy, kde-format
#| msgid "%1 cannot be created."
msgid "Path %1 could not be created."
msgstr "‘%1’ を作成できません。"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:78
#, kde-format
msgid ""
"Failed to integrate your project information into the file %1. The project "
"has not been generated and all temporary files will be removed."
msgstr ""

#. +> trunk5 stable5
#: src/application/generatepage.cpp:85
#, kde-format
msgid "Could not copy template file to %1."
msgstr ""

#. +> trunk5 stable5
#: src/application/generatepage.cpp:119
#, kde-format
msgid "Generation Progress\n"
msgstr "生成の進捗\n"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:220
#, kde-format
msgid "Succeeded.\n"
msgstr "成功。\n"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:224
#, kde-format
msgid "Your project name is: <b>%1</b>, based on the %2 template.<br />"
msgstr ""
"あなたのプロジェクトの名前: <b>%1</b> (テンプレート “%2” に基づく)<br />"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:225
#, kde-format
msgid "Version: %1 <br /><br />"
msgstr "バージョン: %1 <br /><br />"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:226
#, kde-format
msgid "Installed in: %1 <br /><br />"
msgstr "インストール先: %1 <br /><br />"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:227
#, kde-format
msgid ""
"You will find a README in your project folder <b>%1</b><br /> to help you "
"get started with your project."
msgstr ""
"プロジェクトフォルダ ‘%1’ に README があります。<br/>プロジェクトに取り掛かる"
"前にお読みください。"

#. +> trunk5 stable5
#: src/application/generatepage.cpp:233
#, kde-format
msgctxt "@title:window"
msgid "Error"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#. +> trunk5 stable5
#: src/application/introduction.ui:17
#, fuzzy, kde-format
#| msgid ""
#| "This wizard will help you generate a KDE 4 project template.\n"
#| "You will be able to start developing you own KDE 4 project from this "
#| "template."
msgid ""
"This wizard will help you generate a new project.\n"
"You will be able to start developing your own software using Qt and KDE "
"technologies from this template."
msgstr ""
"このウィザードで KDE 4 用プロジェクトのテンプレートを作成します。\n"
"このテンプレートを元にしてあなた自身のプロジェクトの開発を開始することができ"
"ます。"

#. +> trunk5 stable5
#: src/application/kapptemplate.cpp:22
#, fuzzy, kde-format
#| msgid "KDE 4 Template Generator"
msgctxt "@title:window"
msgid "KDE and Qt Template Generator"
msgstr "KDE 4 テンプレートジェネレータ"

#. +> trunk5 stable5
#: src/application/kapptemplate.cpp:44
#, fuzzy, kde-format
#| msgid "Introduction"
msgctxt "@title:tab"
msgid "Introduction"
msgstr "はじめに"

#. +> trunk5 stable5
#: src/application/kapptemplate.cpp:51
#, kde-format
msgid "Set the project properties"
msgstr "プロジェクトのプロパティを設定"

#. +> trunk5 stable5
#: src/application/kapptemplate.cpp:53
#, kde-format
msgctxt "@action:button"
msgid "Generate"
msgstr ""

#. +> trunk5 stable5
#: src/application/kapptemplate.cpp:80
#, fuzzy, kde-format
#| msgid "Your project name is : %1"
msgid "Your project name is: %1"
msgstr "あなたのプロジェクトの名前: %1"

#. i18n: ectx: label, entry (appName), group (Project)
#. +> trunk5 stable5
#: src/application/kapptemplate.kcfg:11
#, kde-format
msgid "Name of the project"
msgstr "プロジェクトの名前"

#. i18n: ectx: label, entry (appVersion), group (Project)
#. +> trunk5 stable5
#: src/application/kapptemplate.kcfg:15
#, kde-format
msgid "Project version"
msgstr "プロジェクトのバージョン"

#. i18n: ectx: label, entry (url), group (Project)
#. +> trunk5 stable5
#: src/application/kapptemplate.kcfg:19
#, kde-format
msgid "Home dir of the user"
msgstr "ユーザのホームディレクトリ"

#. i18n: ectx: label, entry (name), group (User)
#. +> trunk5 stable5
#: src/application/kapptemplate.kcfg:27
#, kde-format
msgid "Name of the user"
msgstr "ユーザの名前"

#. i18n: ectx: label, entry (email), group (User)
#. +> trunk5 stable5
#: src/application/kapptemplate.kcfg:38
#, kde-format
msgid "Email of the user"
msgstr "ユーザのメールアドレス"

#. +> trunk5 stable5
#: src/application/main.cpp:25
#, kde-format
msgid "KAppTemplate"
msgstr "KAppTemplate"

#. +> trunk5 stable5
#: src/application/main.cpp:26
#, fuzzy, kde-format
#| msgid "KAppTemplate is a KDE 4 project template generator"
msgid "KAppTemplate is a KDE project template generator"
msgstr "KAppTemplate は KDE 4 用プロジェクトのテンプレートを生成します"

#. +> trunk5 stable5
#: src/application/main.cpp:28
#, kde-format
msgid "(C) 2008 Anne-Marie Mahfouf"
msgstr "(C) 2008 Anne-Marie Mahfouf"

#. +> trunk5 stable5
#: src/application/main.cpp:30
#, kde-format
msgid "Anne-Marie Mahfouf"
msgstr "Anne-Marie Mahfouf"

#. +> trunk5 stable5
#: src/application/main.cpp:31
#, kde-format
msgid "Sean Wilson"
msgstr "Sean Wilson"

#. +> trunk5 stable5
#: src/application/main.cpp:31
#, kde-format
msgid "Icons from Oxygen Team icons"
msgstr "Oxygen 開発チームによるアイコン"

#. i18n: ectx: property (text), widget (QLabel, versionLabel)
#. +> trunk5 stable5
#: src/application/properties.ui:30
#, fuzzy, kde-format
#| msgid "Version number:"
msgid "&Version number:"
msgstr "バージョン番号:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, kcfg_appVersion)
#. +> trunk5 stable5
#: src/application/properties.ui:40
#, kde-format
msgid "Project's version number"
msgstr "プロジェクトのバージョン番号"

#. i18n: ectx: property (whatsThis), widget (KLineEdit, kcfg_appVersion)
#. +> trunk5 stable5
#: src/application/properties.ui:43
#, kde-format
msgid ""
"Set your project version number. A first project should start with version "
"0.1."
msgstr ""
"あなたのプロジェクトのバージョン番号を設定します。プロジェクトの最初のバー"
"ジョンは 0.1 です。"

#. i18n: ectx: property (inputMask), widget (KLineEdit, kcfg_appVersion)
#. +> trunk5 stable5
#: src/application/properties.ui:46
#, fuzzy, kde-format
#| msgid "0.0; "
msgid "0.0"
msgstr "0.0; "

#. i18n: ectx: property (text), widget (KLineEdit, kcfg_appVersion)
#. +> trunk5 stable5
#: src/application/properties.ui:49
#, kde-format
msgid "0.1"
msgstr "0.1"

#. i18n: ectx: property (text), widget (QLabel, installLabel)
#. +> trunk5 stable5
#: src/application/properties.ui:56
#, fuzzy, kde-format
#| msgid "Installation directory:"
msgid "In&stallation directory:"
msgstr "インストールディレクトリ:"

#. i18n: ectx: property (toolTip), widget (KUrlRequester, kcfg_url)
#. +> trunk5 stable5
#: src/application/properties.ui:66
#, kde-format
msgid "The directory where you will build your project"
msgstr "プロジェクトをビルドするディレクトリ"

#. i18n: ectx: property (whatsThis), widget (KUrlRequester, kcfg_url)
#. +> trunk5 stable5
#: src/application/properties.ui:69
#, kde-format
msgid ""
"Choose the directory where you will build your project. Your home /src is a "
"good default location."
msgstr ""
"あなたのプロジェクトをビルドするディレクトリを選択します。一般的にはホーム"
"ディレクトリの /src が適しています。"

#. i18n: ectx: property (text), widget (QLabel, authorLabel)
#. +> trunk5 stable5
#: src/application/properties.ui:76
#, fuzzy, kde-format
#| msgid "Author name:"
msgid "Author &name:"
msgstr "作者の名前:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, kcfg_name)
#. +> trunk5 stable5
#: src/application/properties.ui:86
#, kde-format
msgid "Your first name and name"
msgstr "あなたの姓名"

#. i18n: ectx: property (whatsThis), widget (KLineEdit, kcfg_name)
#. +> trunk5 stable5
#: src/application/properties.ui:89
#, kde-format
msgid "This will set the copyright to this name"
msgstr "著作権はこの名前に設定されます。"

#. i18n: ectx: property (text), widget (QLabel, emailLabel)
#. +> trunk5 stable5
#: src/application/properties.ui:102
#, fuzzy, kde-format
#| msgid "Author email:"
msgid "A&uthor email:"
msgstr "作者のメールアドレス:"

#. i18n: ectx: property (toolTip), widget (KLineEdit, kcfg_email)
#. +> trunk5 stable5
#: src/application/properties.ui:112
#, kde-format
msgid "Your email address"
msgstr "あなたのメールアドレス"

#. i18n: ectx: property (whatsThis), widget (KLineEdit, kcfg_email)
#. +> trunk5 stable5
#: src/application/properties.ui:115
#, kde-format
msgid ""
"This email address will be next to your name in the copyright credit of the "
"project files."
msgstr ""
"プロジェクトファイルの著作権表示であなたの名前の次に表記されるメールアドレス"
"です。"

#~ msgid "Templates Projects"
#~ msgstr "プロジェクトのテンプレート"

#, fuzzy
#~| msgid "KDE 4 Template Generator"
#~ msgid "KDE and Qt Template Generator"
#~ msgstr "KDE 4 テンプレートジェネレータ"

#~ msgid "Introduction"
#~ msgstr "はじめに"

#~ msgid "Your project name is : %1"
#~ msgstr "あなたのプロジェクトの名前: %1"

#~ msgctxt "Do not translate"
#~ msgid "Preview"
#~ msgstr "Preview"

#~ msgctxt "Do not translate"
#~ msgid "Description"
#~ msgstr "Description"

#~ msgid "The file %1 cannot be created."
#~ msgstr "ファイル ‘%1’ を作成できません。"

#~ msgid ""
#~ "\n"
#~ "\n"
#~ "The file %1 cannot be created."
#~ msgstr ""
#~ "\n"
#~ "\n"
#~ "ファイル ‘%1’ を作成できません。"

#~ msgid "KDE 4 Template Generator"
#~ msgstr "KDE 4 テンプレートジェネレータ"

#~ msgid "KAppTemplate is a KDE 4 project template generator"
#~ msgstr "KAppTemplate は KDE 4 用プロジェクトのテンプレートを生成します"

#~ msgid ""
#~ "This wizard will help you generate a KDE 4 project template.\n"
#~ "You will be able to start developing you own KDE 4 project from this "
#~ "template."
#~ msgstr ""
#~ "このウィザードで KDE 4 用プロジェクトのテンプレートを作成します。\n"
#~ "このテンプレートを元にしてあなた自身のプロジェクトの開発を開始することがで"
#~ "きます。"

#~ msgid "Project Name:"
#~ msgstr "プロジェクト名:"

#~ msgid "Success!\n"
#~ msgstr "成功!\n"

#~ msgid ""
#~ "You will find a README in your project folder <b>%1</b><br /> to help you "
#~ "get started with your project!"
#~ msgstr ""
#~ "プロジェクトフォルダ ‘%1’ に README があります。<br/>プロジェクトに取り掛"
#~ "かる前にお読みください。"

#~ msgid ""
#~ "This wizard will help you generating  a KDE 4 project template.\n"
#~ "You will be able to start developing you own KDE 4 project from this "
#~ "template."
#~ msgstr ""
#~ "このウィザードで KDE 4 用プロジェクトのテンプレートを作成します。\n"
#~ "このテンプレートを元にしてあなた自身のプロジェクトの開発を開始することがで"
#~ "きます。"

#~ msgid "Version Number:"
#~ msgstr "バージョン番号:"

#~ msgid "Installation Directory:"
#~ msgstr "インストールディレクトリ:"

#~ msgid "Author Name:"
#~ msgstr "作者の名前:"

#~ msgid "Author Email:"
#~ msgstr "作者のメールアドレス:"

#~ msgid "TextLabel"
#~ msgstr "TextLabel"
